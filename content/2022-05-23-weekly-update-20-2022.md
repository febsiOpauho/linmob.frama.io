+++
title = "Weekly #LinuxPhone Update (20/2022): A new Sailfish OS device, postmarketOS 21.12 SP5 and PinePhone Pro Camera Progress"
date = "2022-05-23T19:00:00Z"
updated = "2022-05-29T16:31:00Z"
draft = false
[taxonomies]
tags = ["PinePhone","PinePhone Pro","Waydroid","MauiKit","Sailfish OS","DisplayLink","NixOS",]
categories = ["weekly update"]
authors = ["peter"]
[extra]
update_note = "Added nitpick to Waydroid video."
+++

Also: The next Plasma release is inching closer with 5.25 beta, connecting external displays to the Poco F1 with Display Link, some NixOS, and more!
<!-- more -->
_Commentary in italics._

### Software progress
#### GNOME ecosystem
* This Week in GNOME: [#44 Five Across](https://thisweek.gnome.org/posts/2022/05/twig-44/).
* Felipe Borges: [GNOME will be mentoring 9 new contributors in Google Summer of Code 2022!](https://feborg.es/gnome-will-be-mentoring-9-new-contributors-in-google-summer-of-code-2022/).
* Phoronix: [HarfBuzz 4.3 Released With Big Performance Improvements](https://www.phoronix.com/scan.php?page=news_item&px=HarfBuzz-4.3).

#### Plasma/Maui ecosystem
* Nate Graham: [This week in KDE: We don’t like bugs very much](https://pointieststick.com/2022/05/20/this-week-in-kde-we-dont-like-bugs-very-much/).
* MauiKit blog: [Maui 2.1.2 Release](https://mauikit.org/blog/maui-2-1-2-release/).
* KDE Community: [Plasma 5.25 Beta](https://kde.org/announcements/plasma/5/5.24.90/)
-for-testing/). _Help seems to appreciated, so please do!_

#### Sailfish OS
* Jolla Blog: [Meet the Xperia 10 III with Sailfish OS](https://blog.jolla.com/xperia-10-iii/). _Nice! 5G and quite a bit faster!_
* flypig: [Sailfish Community News, 19th May, Xperia 10 III and VoLTE](https://forum.sailfishos.org/t/sailfish-community-news-19th-may-xperia-10-iii-and-volte/11490). _Great news!_

#### Ubuntu Touch
* UBportsNews: [To the UBports Community from your BoD](https://forums.ubports.com/topic/7754/to-the-ubports-community-from-your-bod?_=1653329372806&lang=en-US). _There's messaging that inspires confidence, and ..._

#### Distro news
* [postmarketOS v21.12 Service Pack 5 has been released](https://postmarketos.org/blog/2022/05/15/v21.12.5-release/), featuring a new version of postmarketOS Tweaks, and newer kernels for Qualcomm SDM845 and MSM8996 devices. _A last update for 21.12 before 22.06 is going to come soon._
* Using Mobian and Chatty acts up? [This PSA is should help you find a quick solution](https://mastodon.online/@devrtz/108344777725443773).

#### Apps
* Mardy: [MiTubo 1.0: playlist support, new “website”](http://www.mardy.it/blog/2022/05/mitubo-10-playlist-support-new-website.html).

#### Kernel
* megi's PinePhone Development Log: [Pinephone Pro cameras kernel support](https://xnux.eu/log/#067). _Great! If only one could buy time - I'd love to put together an image to play with the new kernel and Levinboot, but ..._
* CNX Software: [Linux 5.18 release – Main changes, Arm, RISC-V, and MIPS architectures](https://www.cnx-software.com/2022/05/23/linux-5-18-release-main-changes-arm-risc-v-and-mips-architectures/). _Nice roundup!_
* Phoronix: [Qualcomm MSM Driver With Linux 5.19 Adds DSC, Preps For Mesa Driver Within A VM](https://www.phoronix.com/scan.php?page=news_item&px=MSM-For-Linux-5.19-Changes).


### Worth noting
* As he wrote in the "Arch Linux ARM on Mobile" group, Danct12's PinePhone Pro sadly broke. If you're using DanctNIX, this might be a good time to start supporting Danct12 on [Ko-Fi](https://ko-fi.com/danct12), [Patreon](https://patreon.com/Danct12) or [Liberapay](https://liberapay.com/Danct12).
* I had fun with [Geekbench on the PinePhone](https://www.teddit.net/r/pinephone/comments/uqz09x/pinephone_vs_iphone/i8uva4x/?context=3).
* I also partook in a [reddit thread about how to best simulate a Linux Phone for app development](https://teddit.net/r/pinephone/comments/usnbg4/phosh_in_a_virtual_machine/) and... I think this needs thought. _So, think people! (My best idea is just buying a bunch of cheap postmarketOS capable device and sending them around, because that's something I actually could set up. A software solution should scale much better, though!)_
* If you are busy/lazy like me, [testing NixOS on the PinePhone](https://github.com/NixOS/mobile-nixos/pull/468) just got a lot easier!

### Worth reading

#### Tiny secondary displays
* TuxPhones: [Using a Linux phone as a secondary monitor](https://tuxphones.com/howto-linux-as-second-wireless-display-for-linux/). _A tiny monitor, but still :)_

#### Matrix content
* Neko.dev: [Unable to Decrypt a Message on Matrix](https://blog.neko.dev/posts/unable-to-decrypt-matrix.html)

#### Support matters   
* Purism: [Free Software Support Is Critical to Its Success](https://puri.sm/posts/free-software-support-is-critical-to-its-success/). _It's true._

### Worth watching

#### postmarketOS Tweaks meets Sxmo/DWM
* Martijn Braam: [SXMO support in postmarketOS Tweaks](https://www.youtube.com/watch?v=a1kgHRbSFzw). _Color theming for the dwm/X11 based SXMO variant is now a thing. Nice work, Porkyofthepine! Sway support when?_

#### Video out on a device that does not normally support video out
* Alex Pav: [Poco F1 Mobian DisplayLink](https://www.youtube.com/watch?v=5pb9RtGBJws). _Surprisingly smooth!_ Additional information can be found in the [Linux Mainline for POCO F1 Telegram group](https://t.me/linuxpocof1)[^1], rough summary: This was done using the Ubuntu Display Link driver and a modification to the device tree (and compiling the kernel) to enable USB host. The Poco F1 can't supply power over USB-C so you need a Y-cable (or a powered hub) to supply power to the Display Link adapter (Alex used a no-name one aquired at AliExpress).

#### Waydroid on PinePhone Pro
* _Link removed. Crediting work properly matters._
<!-- * Wolf Fur Programming: [Waydroid on the pinephone pro](https://www.youtube.com/watch?v=Fyvz87pSfZY). _Nice video! Nitpick: The original upstream for the Waydroid installer is [MadameMalady](https://github.com/MadameMalady/Manjaro-Waydroid-Manager)!_--> 

#### PinePhone (Pro) Modem Firmware upgrade
* Canadian Bitcoiners: [How To Upgrade The Modem Firmware On The PinePhone Pro](https://www.youtube.com/watch?v=k7dUm3DKkK8).

#### postmarketOS on basically any Android device
* KotCraft Channel Ukraine: [Postmarketos on termux](https://www.youtube.com/watch?v=xh9lijC7i1s).

#### Librem 5 Unboxing
* LINMOB.net: [Unboxing another Librem 5 (and a glimpse at LinuxPhoneApps.org)](https://www.youtube.com/watch?v=A5zyqAGjcSI).x

#### SailfishOS
* Leszek Lesner: [SailfishOS optimize speed by disabling transparency (hack)](https://www.youtube.com/watch?v=4QPk-bVZXyU). _Nice hack!_
* JODC: [Linuxphone Jolla Sailfish OS on Sony Xperia 10 II](https://odysee.com/@JODC:f/LinuxPhone:e). _This is a bit older, I think I missed this once it came out._

#### Ubuntu Touch
* anino207: [Custom patches for Ubuntu Touch | Jerk Installer](https://www.youtube.com/watch?v=dKav1l6RCSE).
* CyberPunked: [WayDroid - Android Apps on Ubuntu Touch - WhatsApp (2022-05-19)](https://www.youtube.com/watch?v=xN6A9SDGqmM). _Spherical sounds!_

#### PinePhone Pro Gaming
* Fast Code Studio: [Minecraft (1.16.5) on PinePhone Pro (Manjaro)](https://www.youtube.com/watch?v=YritdnNAumQ).
    
#### Shorts
* NOT A FBI Honey pot: [RTV - a CLI app where you can browse reddit from the terminal on your pinephone! #shorts](https://www.youtube.com/shorts/E2wTjI9NF5Q).

### Something missing?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please get in touch via social media or email!


[^1]: The group is also bridged to Matrix, but sadly the bridge failed to transfer relevant posts on this topic over to Matrix.

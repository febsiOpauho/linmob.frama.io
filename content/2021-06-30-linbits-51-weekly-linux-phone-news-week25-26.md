+++
title = "LinBits 51: Weekly Linux Phone news / media roundup (week 25/26)"
date = "2021-06-30T21:10:00Z"
updated ="2021-07-01T11:11:00Z"
[taxonomies]
tags = ["PINE64", "PinePhone", "LINMOBapps", "Ubuntu Touch", "postmarketOS", "GTK4", "Phosh", "Librem 5", "PinePhone keyboard", "mutantC",]
categories = ["weekly update"]
authors = ["peter"]
[extra]
update_note = "Fixed NemoMobile link that was wrong initially. Thanks to Jozef Mlich for reporting this error!"
+++

_It's Wednesday. Now what happened since last Wednesday?_

Phosh 0.12.0, accelerated video playback on PinePhone, Nemo Mobile keyboard progress and more!<!-- more --> _Commentary in italics._

### Software releases
* [My GNU Health](https://www.gnuhealth.org/docs/mygnuhealth/) has seen its [1.0](https://invent.kde.org/pim/mygnuhealth/-/tags/v1.0.0) release.
* [gtk-rs has seen a new release](https://gtk-rs.org/blog/2021/06/22/new-release.html), and now has a logo and a website. It's not just any release, as gtk-rs now provides Rust with GTK4 bindings. 
* [Phosh 0.12.0 has been released](https://social.librem.one/@agx/106499109315473158). Now, the proximity sensor will only be enabled during calls _(gamers will appreciate this)_, only adaptive apps will be shown in mobile mode, and more - make sure to read the [full release notes](https://source.puri.sm/Librem5/phosh/-/releases/v0.12.0). 
#### Distributions
* [DanctNIX Arch Linux ARM](https://github.com/dreemurrs-embedded/Pine64-Arch/releases/tag/20210628) has seen another release, bringing all the latest software!

### Worth noting
* [postmarketOS reworked their Downloads pages](https://twitter.com/postmarketOS/status/1409880687608582148). _Lovely!_

### Worth reading

#### Video deconding extravaganza
* Brian Daniels: [Hardware Accelerated Video Decoding on the PinePhone](http://briandaniels.me/2021/06/27/hardware-accelerated-video-playback-on-the-pinephone.html). _This is really impressive! I've actually followed Brian's post to see how well this works, and his measurements check out. I really hope that gstreamer 1.20 is going to land soon and that distributions and app developers manage to make the accelerated video available to us all as soon as possible._

#### Software progress reports
* Jonah Brüchert: [AudioTube nightly builds available](https://jbbgameich.github.io/kde/2021/06/24/audiotube-binaries.html). _AudioTube enthusiasts rejoice!_
* Janet Blackquill: [An Unspecified Amount Of Time In Tok](https://blog.blackquill.cc/an-unspecified-amount-of-time-in-tok). _Further progress for our favourite Kirigami Telegram client!_
* Luis Falcon: [Welcome to MyGNUHealth, the Libre Personal Health Record](https://meanmicio.org/2021/06/24/welcome-to-mygnuhealth-the-libre-personal-health-record/). _This project is just amazing!_
* KDE neon Developers' Blog: [Neon: Maliit Keyboard Now Available on Wayland](https://blog.neon.kde.org/2021/06/24/neon-maliit-keyboard-now-available-on-wayland/). _This might be interesting for tablet users._
* Claudio Cambra: [Figuring out recurrence (and bugs…) in Kalendar’s week 3 (GSoC 2021)](https://claudiocambra.com/2021/06/27/figuring-out-recurrence-in-kalendars-week-3-gsoc-2021/). _I still have not managed to build and run Kalendar on my PinePhone. I really should._
* Kai A. Hiller: [Search Bar of Doom](https://blog.kaialexhiller.de/?p=29). _This is about Fractal Next._
* Sophie's Blog: [New gtk-rs release and more](https://blogs.gnome.org/sophieh/2021/06/25/finally-a-new-gtk-rs-release-and-more/). 
#### Theming
* Purism: [Librem Themes](https://puri.sm/posts/librem-themes/). _Sadly, the Phosh Look app needs to be adjusted as it assumes a certain username._
#### PinePhone hardware keyboard news 
* xnux.eu log: [Pinephone keyboard's final summary](https://xnux.eu/log/#042). _It's final!_

#### Competition ;-) 

LinuxSmartphones: [ News Roundup: Hardware-accelerated video on the PinePhone, themes on the Librem 5](https://linuxsmartphones.com/news-roundup-hardware-accelerated-video-on-the-pinephone-themes-on-the-librem-5/). _Brad's take on recent events. :-)_


### Worth watching

#### Phone reviews
* Chris Titus Tech: [Volla Phone | Using Linux Phone instead of Android or Apple](https://www.youtube.com/watch?v=neG2Z21epLI). _If you were looking for an English language video about the Volla Phone, here you go! (BTW, [here's a notch fix](https://github.com/JamiKettunen/unity8-notch-hax)._

#### Video playback demoed on video
* Brian Daniels: [Hardware Accelerated Video Decoding on the PinePhone](https://www.youtube.com/watch?v=TKQNHalxFF0). _This is the amazing accelerated video playback from the post above!_

#### Tutorial corner
* kdlk.de:tech: [PinePhone Essentials: #Phosh How to Set UI Scale Settings for not optimized apps](https://www.youtube.com/watch?v=W2WuyHiBItY).

#### Sailfish apps
* ARustig View: [GPS Apps on Sailfish OS 4.1.0.24](https://www.youtube.com/watch?v=qAu2PWWz5zQ). _This is really short, but if you ever wondered what Pure Maps looks like on current Sailfish OS, now you know!_

#### Nemo Mobile awesomeness
*  Сергей Чуплыгин: [some progress with nemomobile keyboard](https://www.youtube.com/watch?v=2LdHWEVFmrY). _Yes, this does not look super finished, but let's just appreciate the progress that is happening around Nemo Mobile!_

#### DIY handheld corner
* mutantC: [mutantC v4 compared to others](https://www.youtube.com/watch?v=aKknVVHY1rc).
* mutantC: [mutantC v4 working prototype](https://www.youtube.com/watch?v=6wmJ-LXRhLo).

### Stuff I did

I wrote a blog post on [GNU/Linux on Tablets: Hardware](https://linmob.net/gnu-linux-on-tablets-hardware/). If you want a Linux tablet, make sure to read it and feel free to get in touch if you have questions or feedback.

### Stuff I did

#### Content

I wrote a blog post on [GNU/Linux on Tablets: Hardware](https://linmob.net/gnu-linux-on-tablets-hardware/). I hope to continue this series soon, but can't give any ETA, as I will have to test and thus use more software for this; or at least sit down and write something meaningful about the Lenovo Chromebook Duet.

#### Random
I reworked a few aspects of this site so that it now passes the [validator](https://validator.w3.org/nu/?doc=https%3A%2F%2Flinmob.net%2F). Further work fixed some issues with my Atom feed I noticed in my [Miniflux](https://miniflux.app) instance. Also I made those "Hindsight" notes I wrote while going through my old blog posts visible. You gotta catch them all! ;-)

#### LINMOBapps
LINMOBapps did not get much love this week, again. I only managed to add two apps: 

* [LinMoTube](https://github.com/jakeday/LinMoTube), YouTube Client for Linux Mobile (Python and WxWidgets, if you like try it, [make sure to leave feedback](https://www.reddit.com/r/PINE64official/comments/oacmot/youtube_videomusic_app_for_pinephone_looking_for/)),
* [Phosh Look](https://source.puri.sm/david.hamner/phosh_look), Phosh look manages ~/.config/gtk-3.0 with preset themes.

[Read here what (else) happened](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master). 
Please [do contribute!](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/CONTRIBUTING.md)

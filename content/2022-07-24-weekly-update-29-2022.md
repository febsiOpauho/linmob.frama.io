+++
title = "Weekly GNU-like Mobile Linux Update (29/2022): postmarketOS 22.06.1 for (PinePhone) Pros"
date = "2022-07-24T21:35:00Z"
draft = false
[taxonomies]
tags = ["PinePhone Pro","postmarketOS","GUADEC","DebConf",]
categories = ["weekly update"]
authors = ["peter"]
+++
While [DebConf 22](https://debconf22.debconf.org/) and [GUADEC](https://events.gnome.org/event/77/) are happening,[^1] some more things happened, which is impressive considering the inactivity heat waves cause.
<!-- more -->
_Commentary in italics._

### Software progress

#### GNOME ecosystem
* This Week in GNOME: [#53 GUADEC 2022](https://thisweek.gnome.org/posts/2022/07/twig-53/).
* Philip Withnall: [Mini-GUADEC 2022 in Berlin](https://tecnocode.co.uk/2022/07/21/mini-guadec-2022-in-berlin/).

#### Plasma/Maui ecosystem
* Nate Graham: [This week in KDE: Tons of UI improvements and bugfixes](https://pointieststick.com/2022/07/22/this-week-in-kde-tons-of-ui-improvements-and-bugfixes/).
* Suhaas’ Blog: [GSoC Post 1: FlatpakKCM Update 1](https://jsuhaas22.github.io/devblog/jekyll/update/2022/07/21/gsoc-post-1-flatpakkcm-update.html).
* Volker Krause: [KDE Eco Sprint July 2022](https://www.volkerkrause.eu/2022/07/23/kde-eco-sprint-july-2022.html).
* Fernando Kinoshita: [My week in KDE: slow but steady](https://fhek.gitlab.io/en/my-week-in-kde-slow-but-steady/).
* Qt blog: [Qt Creator 8 released](https://www.qt.io/blog/qt-creator-8-released).

#### Sailfish OS
* In case you're wondering about how open source Sailfish OS's VoLTE Support is, [here's some info on that](https://twitter.com/flypigahoy/status/1551119211652870145)

#### Distributions
* postmarketOS:[postmarketOS // v22.06 SP1: The One Where We Added The Pro & E7](https://postmarketos.org/blog/2022/07/17/v22.06.1-release/). _PinePhone Pro support, yeah! ([Status](https://wiki.postmarketos.org/wiki/PINE64_PinePhone_Pro_(pine64-pinephonepro)))_

#### Acceleration
* Phoronix: [Qualcomm Working On Vulkan Image Processing With New v1.3.222 Extensions](https://www.phoronix.com/news/Vulkan-1.3.222-Released).
* Phoronix: [Adreno 619 Support Added To Mesa - Enables Fairphone 4 GPU Support](https://www.phoronix.com/news/Freedreno-Adreno-619).
* Phoronix: [Linux's Hantro VPU Media Driver Looks To Be Promoted Out Of Staging](https://www.phoronix.com/news/Hantro-VPU-Leaving-Staging). _Nice!_

### Worth noting
* execrable on Purism forums: [Genie-Assistant on L5](https://forums.puri.sm/t/genie-assistant-on-l5/17858). _Genie is the software previously known as [Almond](https://linuxphoneapps.org/apps/edu.stanford.almond.desktop/)._
* If "convergent presenting" is something you're interested in, [you might appreciate this](https://social.librem.one/@agx/108698732578646323)!
* If you're having problems with Phosh's readability while being outside in the sun, [something nice might be coming to help you with that](https://social.librem.one/@agx/108668794144125214)!

### Worth reading

#### Developer Experiences
* Caleb Connolly: [My workflow as an embedded Linux developer](https://connolly.tech/posts/2022_07_20-aosp-vscode/). _Interesting read!_

#### Device reviews
* tendays on Purism forums: [[MyL5] YET Another Librem5 review](https://forums.puri.sm/t/myl5-yet-another-librem5-review/17860).

#### Linux on Tablets
* Neil Brown: [Samsung Galaxy Tab 9.7" and postmarketOS: initial impressions](https://neilzone.co.uk/2022/07/samsung-galaxy-tab-97-and-postmarketos-initial-impressions).
* Neil Brown: [Samsung Galaxy Tab 9.7" and postmarketOS: building a custom image - Neil Brown](https://neilzone.co.uk/2022/07/samsung-galaxy-tab-97-and-postmarketos-building-a-custom-image). _I agree: pmbootstrap is amazing!_

#### Ethics and Interdiction
* Kyle Rankin for Purism: [Purism’s Ethical Marketing Principles](https://puri.sm/posts/purisms-ethical-marketing-principles/). _Ethical refund policy when? It would really help with making r/Purism less of a r/purismhatefest…_
* Kyle Rankin for Purism: [Anti-Interdiction on The Librem 5 USA](https://puri.sm/posts/anti-interdiction-on-the-librem-5-usa/). _[HN Comment thread](https://news.ycombinator.com/item?id=32175637)._

#### Not mobile specific, but _Interesting
* Et tu, Cthulhu: [GNOME at 25: A Health Checkup](https://hpjansson.org/blag/2022/07/23/gnome-at-25-a-health-checkup/). _Great numbers!_

### Worth watching

#### DebConf
* Guido Günther: [The current state of Debian on smartphones](https://meetings-archive.debian.net/pub/debian-meetings/2022/DebConf22/debconf22-231-the-current-state-of-debian-on-smartphones.webm). _Great talk!_

#### Terminal goes GTK4
* baby WOGUE: [Terminal port to GTK4 demo + QUESTION!! GNOME 43](https://www.youtube.com/watch?v=tG4bUDNrbPU).

#### Ubuntu Touch
* TechSecurity: [installing Ubuntu touch in redmi mi 4x](https://www.youtube.com/watch?v=axrkg6ZalJI).

#### Sailfish OS
* BohdanKoles: [Sailfish OS – REAL review and usage experience](https://www.youtube.com/watch?v=Kc9MXDGFNbA). _Detailed and well explained!_
* Xpain: [[RAW] Installing Sailfish OS On A Samsung Galaxy S4.](https://www.youtube.com/watch?v=qzAJVo41_Xo). _Shaky!_

### Something missing?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please get in touch via social media or email!

PS: In case you are wondering about the title [...](https://fosstodon.org/@linmob/108516506484897358)

10110

[^1]: GUADEC coverage is going to happen with the next Weekly Update. Sorry for that!

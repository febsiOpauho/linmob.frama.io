+++
title = "Weekly GNU-like Mobile Linux Update (4/2023): Unofficial Ubuntu Touch on the PinePhone Pro and a Plasma Mobile update"
draft = false
date = "2023-01-30T22:26:00Z"
[taxonomies]
tags = ["Sailfish OS", "PinePhone Pro", "Ubuntu Touch", "Nemo Mobile", "Maemo Leste", "Plasma Mobile", "GNOME Shell on Mobile"]
categories = ["weekly update"]
authors = ["peter"]
[extra]
author_extra = " (with friendly assistance from plata's awesome script)"
+++

Also: A video detailing the current state of GNOME Shell on Mobile, a monthly Nemo Mobile update, Maemo Leste gains GUI calling, and more!
<!-- more -->
_Commentary in italics._

### Software progress

#### GNOME ecosystem
- This Week in GNOME: [#80 Different Locales](https://thisweek.gnome.org/posts/2023/01/twig-80/)
- feborg.es: [Visit us in Brno for Linux App Summit 2023!](https://feborg.es/visit-us-in-brno-for-las-2023/)
- jrb: [Crosswords 0.3.7: Adaptive Layout, Animations, and Arrows](https://blogs.gnome.org/jrb/2023/01/23/crosswords-0-3-7-adaptive-layout-animations-and-arrows/)

#### Plasma ecosystem	
- Nate Graham: [This week in KDE: Major bugfixing and screen recording in Spectacle](https://pointieststick.com/2023/01/27/this-week-in-kde-major-bugfixing-and-screen-recording-in-spectacle/)
- Plasma Mobile: [The Dev Log: January 2023](https://plasma-mobile.org/2023/01/30/january-blog-post/)

#### Ubuntu Touch
- u/oklopfer: [I ported Ubuntu Touch to the PinePhone Pro, and you can try it out now!](https://www.reddit.com/r/PINE64official/comments/10j2i63/i_ported_ubuntu_touch_to_the_pinephone_pro_and/) _Grab the [latest release here](https://gitlab.com/ook37/pinephone-pro-debos/-/releases) and give it a try!_


#### Sailfish OS
- [Sailfish Community News, 26th January, Home Automation](https://forum.sailfishos.org/t/sailfish-community-news-26th-january-home-automation/14194)
- [Sailfish OS News Network: "#PureMaps 3.2.0 released for #SailfishOS. Available on chum (https://openrepos.net/content/olf/sailfishoschum-gui-installer) and the Jolla store."](https://mastodon.social/@sailfishosnews/109776864080858146)

#### Nemo Mobile
- Nemo Mobile UX team: [Nemomobile in January 2023](https://nemomobile.net/pages/nemomobile-in-january-2023/)
- In case you want to try the latest stuff: [Jozef Mlich: "I have updated #NemoMobile -git image. Fresh rebuild is for PinePhone, PinePhone Pro, PineTab, and CutiePi. The images are at https://nemo.mlich.cz/images/. Let me know if it boots into UI.…"](https://fosstodon.org/@jmlich/109767379519580242)

#### Maemo Leste
- [Maemo Leste on Twitter: "sphone with Telepathy backend, works for regular calls and sip, although sip needs a bit of work. Hopefully will work for XMPP soon too. More updates will follow soon, but this allows us finally start using conversations for SMS as well. More updates later this week!" / Twitter](https://twitter.com/maemoleste/status/1617692464000937985)

#### Capyloon
- [Capyloon: "Capyloon recently got support for themeing, so we're calling for help from designers that are interested in creating the best looking OS UI ever! Your creativity is the limit :) …"](https://fosstodon.org/@capyloon/109774278435438085)

#### Distributions
- Breaking updates in pmOS edge: [Crashes in GTK applications on the PinePhone](https://postmarketos.org/edge/2023/01/24/gtk-crashes/)

#### Stack
- Phoronix: [PipeWire 0.3.65 Adds New Combine-Stream Module, Bluetooth MIDI](https://www.phoronix.com/news/PipeWire-0.3.65-Released)
- Phoronix: [MPV Player 0.35.1 Released With Wayland & PipeWire Fixes](https://www.phoronix.com/news/MPV-Player-0.35.1-Released)

#### Linux
- Phoronix: [Linux 6.2-rc6 Released & It's Suspiciously Small](https://www.phoronix.com/news/Linux-6.2-rc6-Released)

#### Matrix
- Matrix.org: [This Week in Matrix 2023-01-27](https://matrix.org/blog/2023/01/27/this-week-in-matrix-2023-01-27)

### Worth noting
- MobileLinux: [WebKitGTK GPU Acceleration added to Droidian - @eugenio_g7](https://www.reddit.com/r/mobilelinux/comments/10jvqn4/webkitgtk_gpu_acceleration_added_to_droidian/) _Impressive!_
- If you've been wondering, why camera support is such a rare feature across devices supported by postmarketOS, here's a pretty good answer: [Dylan Van Assche: "@rapto Yes, the camera stack i…" - Fosstodon](https://fosstodon.org/@dylanvanassche/109754266988879284)



### Worth reading
- TuxPhones.com: [Linux phones are not automatically secure](https://tuxphones.com/linux-mobile-devices-are-not-inherently-secure/). _Great write-up!_
- Plasma Mobile: [The Dev Log: January 2023](https://plasma-mobile.org/2023/01/30/january-blog-post/). _Great progress!_
- Hamblingreen: [Sxmo Customization](https://hamblingreen.com/2023/01/23/sxmo-customization.html). _Nice write up!_
- OMG Linux: [New Video Gives Early Look at GNOME Shell for Mobile Devices](https://www.omglinux.com/gnome-shell-mobile-hands-on-video/)
- Mobian Blog: [Off-topic: The importance of efficient tooling](https://blog.mobian.org/posts/2023/01/25/efficient-tooling/). _Guess who has such a RISC-Vy board ..._

### Worth listening
- postmarketOS Podcast: [#27 Chromebooks, SDM845 Wakeup for Calls, Installing pmOS, dnsane](https://cast.postmarketos.org/episode/27-Chromebooks-SDM845-Wakeup-Installing-pmOS-dnsane/)

### Worth watching[^1]
- Cassidy Blaede: [GNOME Shell on Mobile!](https://www.youtube.com/watch?v=yJq8Cq9LixE)
- Cassidy Blaede: [“Done” from Flathub on GNOME Mobile (1/2)](https://www.youtube.com/watch?v=3HR7ZNleLE4)
- Cassidy Blaede: [“Done” from Flathub on GNOME Mobile (2/2)](https://www.youtube.com/watch?v=qLem0N0L1hM)
- Akademy: [Plasma Mobile in 2022](https://tube.kockatoo.org/w/ffskwp2THa8thacav8ezDk), [YouTube](https://www.youtube.com/watch?v=bBiQgjrBQAk)
- phalio: [PinePhone + Keyboard with Charger/Dock in Phone](https://diode.zone/w/1056cf39-5e51-495e-b3de-95644f378198)
- Ausfaller: [Pinephone Pro Explorer Edition 1 Year later review](https://www.youtube.com/watch?v=wia-ThStoJ0)
- fossfrog: [USB Arsenal for PinePhone in Kali Linux](https://www.youtube.com/watch?v=qSS1kIJJBpk)
- Lup Yuen Lee: [#LVGL Terminal for #PinePhone on Apache #NuttX RTOS](https://www.youtube.com/watch?v=WdiXaMK8cNw)
- DHOCNET TV: [PostmarketOS Usage Test | Xiaomi Redmi 2 Prime | WT88047 2GB RAM 16GB ROM](https://www.youtube.com/watch?v=ib9b8p7ep0Y)
- Continuum Gaming: [Microsoft Continuum Gaming E349: Do the most popular Android apps work on SFOS with AD?](https://www.youtube.com/watch?v=Ia0EDlXgMQE)
- Linux Guides EN: [How to install Ubuntu Touch on Fairphone 2 - Overview](https://www.youtube.com/watch?v=tPx3wfT8nOE)
- Алексей П: [postmarketOS Linux](https://www.youtube.com/watch?v=8vlhZc7J2x8)
- Novaspirit Tech: [Checking out Plasma Mobile](https://www.youtube.com/watch?v=FhW7nSE6CaI)

### Thanks

Huge thanks again to Plata for [the nifty set of Python scripts](https://framagit.org/linmob/linmob.frama.io/-/merge_requests/5) that speeds up collecting links from feeds by a lot.

### Something missing? Want to contribute?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please just put it into [the hedgedoc pad](https://pad.hacc.space/7yCLy5a9QyOLWusIFiTt9A?edit) for the next one! __If you just stumble on a thing, please put it in there too - all help is appreciated!__

Also, [help with getting this blog relisted on Bing is appreciated](https://fosstodon.org/@linmob/109726744903010450)

PS: I'm looking for feedback - [what do you think?](mailto:weekly-update@linmob.net?subject=Feedback%20on%20Weekly%20Update)

[^1]: Due to a lack of time, I have no idea whether these are actually worth watching. Sorry!

+++
title = "Weekly Update (4/2022): New releases of Phosh, phoc, Squeekboard and Chatty (with MMS), suspend on the PinePhone Pro and GStreamer 1.20 RC1"
date = "2022-01-28T22:15:00Z"
draft = false
[taxonomies]
tags = ["PinePhone","PinePhone Pro","PinePhone Keyboard","Minimum Viable Computer","Phosh","Chatty","MMS","Nemo Mobile",]
categories = ["weekly update"]
authors = ["peter"]
+++

In the fourth week of 2022, we're having an overhaul of the GNOME on Mobile stack delivering important features like MMS or arbitrary (as in non-numeric) passwords, a report on what Nemo Mobile accomplished in January, and three PinePhone gaming videos!
<!-- more --> _Commentary in italics._ 

### Editorial
While I am writing these lines, I have a _PinePhone Pro_ a few meters away in my backpack with me. It's still in its box though, as I want to make a proper unboxing video – but as I am not at home, this can't happen today. The odd thing is: It feels good to not rush into it. Being able to take time, to maybe read up a bit about what's supposed to be working and what current workarounds are, is a nice luxury.[^1] 
Looking at the small box that contains the PinePhone Pro makes me think of the Librem 5 (and its big box) I ordered 1559 days ago and that likely is going to arrive some time this year. How fun would it be if it arrived precisely 1600 days after ordering?
But that's enough rambling. Have fun reading what happened in the past seven days!

### Hardware news
* Brian Benschoff: [A Minimum Viable Computer, or Linux for $15](https://bbenchoff.github.io/pages/LinuxDevice.html) (via [Liliputing](https://liliputing.com/2022/01/this-minimum-viable-computer-is-a-pocket-sized-pc-that-could-cost-15-to-make.html)).

### Hardware enablement news
* PinePhone Pro hardware enablement has been making progress:
  * With this [new build of uboot, suspend is coming to Manjaro on the PinePhone Pro](https://gitlab.manjaro.org/manjaro-arm/packages/core/uboot-pinephonepro/-/jobs/7393), which is huge news for battery life. [And it's not just Manjaro](https://fosstodon.org/@martijnbraam/107701871046054627).
  * USB-C video out [can also work (here on Mobian)](https://twitter.com/benpocalypse/status/1487112137881305091).

### Software news
#### GNOME ecosystem
* [Phosh 0.15.0 is out](https://gitlab.gnome.org/World/Phosh/phosh/-/releases#v0.15.0), delivering swipeable notification frames; VPN quicksettings, authentication and status icon; and support for arbitrary passwords among other things. _One of my favorites is that the night light feature is now working. Soo nice when tired._
* [Phoc 0.12.0 was also released](https://gitlab.gnome.org/World/Phosh/phoc/-/releases/v0.12.0). 
* [Squeekboard 1.16 joined the group](https://gitlab.gnome.org/World/Phosh/squeekboard/-/commit/d49ce45de0956432cef9b957f806d9377fee4bc0), supporting a few new languages and delivering multiple small fixes.
* [Chatty 0.6.0 also landed](https://source.puri.sm/Librem5/chatty/-/commit/da26b33a98137e542efd840eadae426b0cdb7b42), delivering MMS for those who need it._
* Also, [GNOME Calls 42.beta.1](https://gitlab.gnome.org/GNOME/calls/-/releases#42.beta.1) has been released, delivering SIP improvements.
* Julian Sparber: [A Long Overdue Update – Fractal-next](https://blogs.gnome.org/jsparber/2022/01/28/a-long-overdue-update-fractal-next/). _
* This Week in GNOME: [#28 PrintScrn](https://thisweek.gnome.org/posts/2022/01/twig-28/).

#### Plasma/Maui ecosystem
* Dan Johansen: [How to test Plasma 5.24 beta on Plasma Mobile](https://blog.strits.dk/how-to-test-plasma-5-24-beta-on-plasma-mobile/). _Give it a try!_
* KDE.news: [Season of KDE Kicks Off](https://dot.kde.org/2022/01/26/season-kde-kicks). _I am really looking forward to notifications in Tokodon and flatpak permission management in Discover!_

#### GStreamer
* Phoronix: [GStreamer 1.20 RC1 Released With Many Exciting Improvements](https://www.phoronix.com/scan.php?page=news_item&px=GStreamer-1.20-RC1). _GStreamer 1.20 is going to be super exciting for Cedrus and Hantro VPU support alone!_

#### Distro releases
* Danct12 has released [new images of his Arch Linux ARM based distribution](https://github.com/dreemurrs-embedded/Pine64-Arch/releases/tag/20220124) for the first time this year. _Since the release, the new versions of Phosh, phoc, Squeekboard and Chatty have landed!_
* [Manjaro Phosh Beta 21](https://github.com/manjaro-pinephone/phosh/releases/tag/beta21) and
* [Manjaro Plasma Mobile Beta 10](https://github.com/manjaro-pinephone/plasma-mobile/releases/tag/beta10) have also been released.
* Nemo Mobile have also released [new images for the PinePhone and PinePhone Pro](https://img.nemomobile.net/2022.05/). _It's supposed to stabilize until May, hence the number._

### Worth reading

#### Nemo Mobile
* Nemo Mobile: [NemoMobile in January/2022](https://nemomobile.net/pages/nemomobile-in-january-2022/).

#### PinePhone Keyboard 
* xnux.eu log: [Pinephone Keybaord – kernel driver merged](https://xnux.eu/log/#058).
* presire: [PinePhone Keyboard Driver Installation for Mobian](https://github.com/presire/PPKeyboard_How_to_Install).

#### PinePhone usage reports
* /u/bloggerdan: [Daily Driving Plasma Mobile Part 2](https://www.reddit.com/r/PinePhoneOfficial/comments/sbricv/daily_driving_plasma_mobile_part_2/).

#### Rust-y cute Developments
* Ayush Singh: [Creating Rust/QML Project](https://www.programmershideaway.xyz/posts/post1/).

#### Ubuntu Touch
* UBports blog: [Ubuntu Touch Q&A 115](https://ubports.com/blog/ubports-news-1/post/ubuntu-touch-q-a-115-3800). _The written summary of the past Q&A. Make sure to join Q&A 116 on January 29th, 7 pm UTC!_

#### Purism
* Todd Weaver for Purism: [Purism 2022 Roadmap](https://puri.sm/posts/purism-2022-roadmap/). _I didn't know roadmaps are even allowed to be this vague._

### Worth listening
* PineTalk: [Season 2 Episode 5: Talk About a Dense 15 Minutes](https://www.pine64.org/2022/01/26/6118/). _Nice one!_

### Worth watching
#### PinePhone (Pro) Gaming
*  Raezroth Helmiem: [PinePhone Pro running Portal 2 through Steam using Box86](https://www.youtube.com/watch?v=yPr0Aw3xZrA).
* CalcProgrammer1: [Half Life 2 Deathmatch and Half Life 2 on PinePhone Pro with Box86](https://www.youtube.com/watch?v=lAfEB0B14fw).
* Grhmhome: [pico 8 on the Pinephone](https://www.youtube.com/watch?v=uKGJQW24fHU).

#### PinePhone Pro Experiments
* Danct12: [Compiz running on PinePhone Pro](https://www.youtube.com/watch?v=6E0kOIWVI7A). _Danct12's video description is very, very fitting._

#### PinePhone Criticism
* HyperVegan: [Pinephone With Mobian As Daily Driver In 2021](https://odysee.com/@HyperVegan:2/20220125_FinallyTalkingAboutPinephone_720p30fps:5). _Feedback on things that happened months ago is rarely really useful._

#### Cute Tablets
* ETA PRIME: [This New Open Source CM4 Powered Linux Tablet Is Awesome! Hands-On With The Cutie Pi](https://www.youtube.com/watch?v=FDMYBrYk-aY).

#### Bootloaders
* Martijn Braam: [Tow-Boot on the Pinebook Pro](https://spacepub.space/videos/watch/b166d197-e755-47c9-9bbe-03ad524c0205). _[Tow-Boot](https://tow-boot.org) is a cool project that works on making booting ARM devices boring. While boring may sound ... um ... boring, in this case it's actually super exciting to have a more standardized approach on ARM, as it makes work for distributions a lot easier._

#### PINE64 Quarterly Community Q&A
* PINE64: [Quarterly Community Q&A session [Live recording]](https://www.youtube.com/watch?v=tORlxpzmF3U). _A long listen, but worth it, e.g. to learn about a PinePhone NFC back cover._

### Something missing?
If your projects' cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please get in touch via social media or email!

[^1]: Coincidentally, with my [currently used PeerTube instance going offline soon](https://devtube.dev-wiiki.de), I am glad to delay the moment of "okay, I do need to fix this and find a new instance" for a few more hours. I am wondering if I should not just take some VPS and put raw video files on there, files in a folder, FOSDEM style – I don't want to run a PeerTube instance myself, and I dislike moving instances (and fear this might occur again). Suggestions on this issue are very much welcome!

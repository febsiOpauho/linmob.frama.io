+++
title = "Weekly GNU-like Mobile Linux Update (51/2022): The last one to be published in 2022"
date = "2022-12-26T23:59:13Z"
draft = false
[taxonomies]
tags = ["Ubuntu Touch","PineTab2","PinePhone Pro","RISC-V",]
categories = ["weekly update"]
authors = ["peter"]
[extra]
author_extra = " (with friendly assistance from plata's awesome script)"
update_note = "Added TuxPhones article I missed due to collecting feeds a bit early."
+++

Ubuntu Touch 20.04 beta is the elephant in the room, but there's more!
<!-- more -->

_Commentary in italics._

### Hardware
- [Sipeed Lichee Pi 4A is a modular RISC-V mini PC with up to 16GB RAM - Liliputing](https://liliputing.com/sipeed-lichee-pi-4a-is-a-modular-risc-v-mini-pc-with-up-to-16gb-ram/) _The noteworthy bit is the Lichee Phone 4A, obviously._

### Software progress

#### GNOME ecosystem
- This Week in GNOME: [#75 Redesigned Sound](https://thisweek.gnome.org/posts/2022/12/twig-75/)
- nibblestew: [After exactly 10 years, Meson 1.0.0 is out](https://nibblestew.blogspot.com/2022/12/after-exactly-10-years-meson-100-is-out.html)
- Felipe Borges: [Proposing internship project ideas](https://feborg.es/proposing-internship-project-ideas/)

#### Plasma ecosystem	
- Nate Graham: [This week in KDE: Holiday features](https://pointieststick.com/2022/12/23/this-week-in-kde-holiday-features/)
- TSDGeos: [Donate to KDE with a 10% power up! (1-week-offer)](https://tsdgeos.blogspot.com/2022/12/donate-to-kde-with-10-power-up-1-week.html)
- KDAB: [The Top 100 QML Resources by KDAB](https://www.kdab.com/top-100-qml-resources-kdab/)

#### Nemo Mobile
- [Jozef Mlich: "I was playing with glacier-messages today. Now it is showing time of message"](https://fosstodon.org/@jmlich/109552924640693182)

#### Ubuntu Touch
- [Merry Christmas from UBports | UBports Forum](https://forums.ubports.com/topic/8446/merry-christmas-from-ubports). _20.04 beta available in the RC channel! Yay!_
  - Lemmy - linuxphones: [Ubuntu Touch 20.04 Focal Fossa now has a beta Release Candidate update channel](https://lemmy.ml/post/675418)
  - [Marius Gripsgard: "Ubuntu touch 20.04 beta/rc is out! Get it while it's fresh"](https://fosstodon.org/@mariogrip/109571727690430543)
#### Sailfish OS
- Adam Pigg worked on the Sailfish OS on PinePhone documentation: [Home · sailfish-on-dontbeevil/documentation Wiki · GitHub](https://github.com/sailfish-on-dontbeevil/documentation/wiki)

#### Distributions
- Kupfer: [v0.2.0-rc1: Versioned Kupferbootstrap docs](https://kupfer.gitlab.io/blog/2022-12-20_v0-2-0-rc1_versioned-kbs-docs.html) 
- Manjaro PinePhone Plasma Mobile: [Factory Image 202212201610](https://github.com/manjaro-pinephone/plasma-mobile/releases/tag/factory_202212201610)
- Manjaro PinePhone Plasma Mobile: [Beta 14](https://github.com/manjaro-pinephone/plasma-mobile/releases/tag/beta14)

#### Stack

#### Non-Linux
- Lup Yuen Lee: [NuttX RTOS for PinePhone: Display Engine](https://lupyuen.github.io/articles/de3) 

#### Matrix
- Matrix.org: [The Matrix Holiday Update 2022](https://matrix.org/blog/2022/12/25/the-matrix-holiday-update-2022)
- Matrix.org: [This Week in Matrix 2022-12-23](https://matrix.org/blog/2022/12/23/this-week-in-matrix-2022-12-23)

### Worth noting
- [NeoChat: "Quotient 0.7 is out! Containing a few years of work, this releases makes it possible to use end-to end-encryption with NeoChat and bring other nice features and improvements with it."](https://fosstodon.org/@neochat/109546958582325342)
- A few items on the PinePhone Pro:
  - [Robert Mader: "Right now I'm taking a little …" - FLOSS.social](https://floss.social/@rmader/109541967453636222) _Camera!_ 
  - [Robert Mader: "Short thread on news on Pinephone Pro Developments" - FLOSS.social](https://floss.social/@rmader/109575429582503961) _Upstream work!_
  - [Javier Martinez C.: "#Fedora 37 booting a #Linux mainline #kernel plus the patch-set that adds display and touchscreen support for the #PinePhonePro." - Fosstodon](https://fosstodon.org/@javierm/109580362870936797)
  - [Gerben Jan Dijkman: "For all @gentoo users of the…" - GJDWebserver Mastodon](https://hub.gjdwebserver.nl/@gjdijkman/109553776351870345)

  - [chfkch: "#NixOS on the @PINE64 #PinePhonePro" - ruhr.social](https://ruhr.social/@chfkch/109580347332290916)
- Speaking of NixOS: 
  - [Project Insanity: "Not sure if this is going to work but we're trying to package #Lumiri (unity8) of #UbuntuTouch for #NixOS…" - chaos.social](https://chaos.social/@pi_crew/109551240182261153)
- Finally: The end of JavaScript-rendered lists is nigh: [LinuxPhoneApps.org: "Look at that: https://linuxpho…" - LinuxRocks.Online](https://linuxrocks.online/@linuxphoneapps/109576732699172470)

### Worth reading
- TuxPhones - Linux phones, tablets and portable devices: [The PineTab2 is a new, faster Linux tablet - and it's not alone](https://tuxphones.com/pinetab2-rk3586-linux-tablet-juno-tablet-fydetab-duo/)
- Purism: [Purism and Linux 5.19 to 6.1](https://puri.sm/posts/purism-and-linux-5-19-to-6-1/)
- Purism: [Where is My Librem 5? Part 2](https://puri.sm/posts/where-is-my-librem-5-part-2/)
- Purism: [Latest Improvements in Purism’s Privacy-first Cellular Plans](https://puri.sm/posts/latest-improvements-in-purisms-privacy-first-cellular-plans/)
- LinuxPhoneApps.org: [Help us achieve self-maintaining App Listings by adding AppStream Metadata!](https://linuxphoneapps.org/blog/please-help-adding-appstream-metadata/) _Please!_

### Worth watching
- Sab GFX: [PostmarketOS 22.12 Has Been Released - A Linux Distribution For Phones | Linux News with Sab GFX](https://www.youtube.com/watch?v=iNUYYUuUUTY)
- Jozef Mlich: [Entering PIN with Nemomobile](https://www.youtube.com/watch?v=ZGL0q-oaFjQ)
- Sailfish official: [Pixel 3a ubuntu touch install waydroid gapps](https://www.youtube.com/watch?v=IgnB6BI6E5M)
- CyberPunked: [Waydroid helper app - install / uninstall on Ubuntu Touch - android apps](https://www.youtube.com/watch?v=e5yM7XEXryg)
- Repair A2Z: [POCO F1 Window+Linux Dual Boot | Poco F1 Window+Ubuntu Dual Boot | Poco F1 Window+Android Dual Boot](https://www.youtube.com/watch?v=CriChXBmEas)
- ernmander: [Mastodon Client on Ubuntu Touch](https://www.youtube.com/watch?v=NremxlwZIrI)
- Todd Weaver: [An Interlude of Interfacing with Interledger](https://www.youtube.com/watch?v=Whp4RfW3K_U&t=17156s). _I did not have the time to look at this, but since I am so happy about all contributions, have fun - and thank you, anonymous contributor!__ 
- Linux Guides DE: [Ubuntu Touch getestet - Für wen ist es geeignet? Was hat sich in den letzten Jahren getan?](https://www.youtube.com/watch?v=olNfttuk5yU) _German._
- Twinntech: [Pinephone the lie continues as the koolaid is drunk.](https://www.youtube.com/watch?v=b9yt05B7RyY). _Don't engage!_ [^1]

### Thanks

Huge thanks again to Plata for [a nifty set of Python scripts](https://framagit.org/linmob/linmob.frama.io/-/merge_requests/5) that speeds up collecting links from feeds by a lot.

### Something missing? Want to contribute?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please just put it into [the hedgedoc pad](https://pad.hacc.space/7yCLy5a9QyOLWusIFiTt9A) for the next one! __If you just stumble on a thing, please put it in there too - all help is appreciated!__

PS: I'm looking for feedback - [what do you think?](mailto:weekly-update@linmob.net?subject=Feedback%20on%20Weekly%20Update)


[^1]: Trust me, I've tried, it's not worth it. IIRC Twinntech basically admits to have done cherry-picking when they read the PinePhone product description... You can't help people like that. So, don't fall into the [XKCD 386 trap](https://xkcd.com/386), and do something better with your time!


+++
title = "Phoronix: Open source Tegra drivers soon in Linux 3.8"
aliases = ["2012/11/28/phoronix-open-source-tegra-drivers.html"]
author = "peter"
date = "2012-11-28T18:12:00Z"
layout = "post"
[taxonomies]
tags = ["drivers", "linux", "nvidia", "nvidia tegra", "Link", "tegra", "tegra2", "Tegra3"]
categories = ["shortform", "shortform"]
authors = ["peter"]
+++
<a href='http://www.phoronix.com/scan.php?page=news_item&amp;px=MTIzODA'>Phoronix: Open source Tegra drivers soon in Linux 3.8</a>

And I have always avoided Tegra since the LG Optimus 2X because of NVidia's drivers.

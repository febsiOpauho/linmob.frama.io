+++
title = "Weekly GNU-like Mobile Linux Update (37/2022): libadwaita 1.2, Sxmo 1.11.1 and much more"
date = "2022-09-18T21:07:00Z"
draft = false
[taxonomies]
tags = ["PinePhone", "PinePhone Pro", "GNOME Shell", "libadwaita 1.2", "Plasma Mobile", "Librem 5", "Phosh", "Squeekboard", "Sxmo"]
categories = ["weekly update"]
authors = ["peter"]
[extra]
author_extra = " + Matthew Fennell, hamblinggreen and (with friendly assistance from plata's awesome script)"
+++
GNOME 43 getting closer to release: libadwaita and libhandy have been updated! The KDE folks work hard on Plasma 5.26 and Plasma Mobile Gear 22.09, Manjaro have released a new Phosh beta for PinePhone (Pro) and people have been playing with the mobile variant of GNOME Shell!
<!-- more -->
_Commentary in italics._

### Software progress

#### GNOME ecosystem
- This Week in GNOME: [#61 Overview Tabs](https://thisweek.gnome.org/posts/2022/09/twig-61/). _Nice improvements to Flare and Cawbird!_
- Alexander Mikhaylenko: [Libadwaita 1.2](https://blogs.gnome.org/alexm/2022/09/15/libadwaita-1-2/). _Great improvements, really looking forward to see these landing in Apps!_
- Ignacy Kuchciński: [GSoC 2022: Overview](https://ignapk.blogspot.com/2022/09/gsoc-2022-overview.html). _Nice Nautilus improvements!_
- Sophie's Blog: [GUADEC and App Organization BoF](https://blogs.gnome.org/sophieh/2022/09/16/guadec-and-app-organization-bof/)
- Jakub Steiner: [Even Mo’ Pixels](https://blog.jimmac.eu/2022/evenmopixels/)
- Allan Day: [gnome-info-collect closing soon](https://blogs.gnome.org/aday/2022/09/15/gnome-info-collect-closing-soon/)
- Julian Sparber: [Mini GUADEC Berlin 2022](https://blogs.gnome.org/jsparber/2022/09/12/mini-guadec-berlin-2022/)

##### Notable Software releases
* [GTK 4.8.1](https://gitlab.gnome.org/GNOME/gtk/-/commit/3a941eff4aa0ad66e0aa34e6528bc4d78c5e5d5e). _Bugfixes and translations._
* [libhandy 1.8](https://gitlab.gnome.org/GNOME/libhandy/-/commit/fce63fcc85621c33dab85e32b349d707bf6d1552#9f621eb5fd3bcb2fa5c7bd228c9b1ad42edc46c8)
* [libadwaita 1.2](https://gitlab.gnome.org/GNOME/libadwaita/-/commit/a905117bd2150de9e85d65f8cdce8d8fb001b89e). _Make sure to read Alex's post above!_
- [Squeekboard 1.20.0](https://forums.puri.sm/t/squeekboard-1-20-0-released/18226)

#### Plasma/Maui ecosystem
- Nate Graham: [This week in KDE: It’s a big one, folks](https://pointieststick.com/2022/09/16/this-week-in-kde-its-a-big-one-folks/)
- KDE Announcements: [Plasma 5.26 Beta](https://kde.org/announcements/plasma/5/5.25.90/)
  - Phoronix: [KDE Plasma 5.26 Beta Released With New "Plasma Bigscreen" Interface For TVs](https://www.phoronix.com/news/KDE-Plasma-5.26-Beta)
- KDE Announcments: [KDE Ships Frameworks 5.98.0](https://kde.org/announcements/frameworks/5/5.98.0/)
- tsdgeos: [Come to Barcelona for Akademy-es 2022!](https://tsdgeos.blogspot.com/2022/09/come-to-barcelona-for-akademy-es-2022.html)
- KDE Neon Blog: [Plasma 5.26 Beta Testing](https://blog.neon.kde.org/2022/09/16/plasma-5-26-beta-testing/)
- Claudio Cambra: [KDE PIM in July and August](https://claudiocambra.com/2022/09/15/kde-pim-in-july-and-august/). _Great roundup!_
- Planet KDE: [GSOC Update 4](https://rojas.run/posts/gsoc-update4/)
- Phoronix: [Qt 6.4 Release Candidate Arrives With Added Modules For 3D Physics, HTTP Server](https://www.phoronix.com/news/Qt-6.4-RC1-Released)

#### Sxmo
* [Sxmo 1.11.1 released — sourcehut lists](https://lists.sr.ht/~mil/sxmo-announce/%3C87k064v8v0.fsf%40momi.ca%3E)

#### Ubuntu Touch
* [UBports Biweekly Newsletter](http://ubports.com/blog/ubports-news-1/post/knock-knock-who-s-there-it-s-your-biweekly-ubports-newsletter-3865)


#### Distributions
* [PostmarketOS now includes experimental support for Gnome Mobile UI](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3404)
* [Mobian now has early beta support for sxmo](https://wiki.mobian-project.org/doku.php?id=desktopenvironments&rev=1663400489&do=diff)
* [Manjaro ARM Phosh Beta 27 Released](https://github.com/manjaro-pinephone/phosh/releases/tag/beta27). _Aside from the usual "newer releases of almost anything" this includes a variant of Megapixels with early support for the PinePhone Pro._
* Breaking updates in pmOS edge: [GNOME: gnome-shell gets uninstalled on upgrade](https://postmarketos.org/edge/2022/09/16/gnome/)

#### Matrix
* Matrix.org: [This Week in Matrix 2022-09-16](https://matrix.org/blog/2022/09/16/this-week-in-matrix-2022-09-16)

#### Stack
- Phoronix: [Linux's Display Brightness/Backlight Interface Is Finally Being Overhauled](https://www.phoronix.com/news/2022-Linux-Backlight-Overhaul)
- Phoronix: [HarfBuzz 5.2 Released With Unicode 15 Support](https://www.phoronix.com/news/HarfBuzz-5.2-Released)


### Worth noting
- PINE64official (Reddit): [Apache NuttX RTOS Online Workshop (PineCone BL602 / PineDio Stack BL604 / 24-25 Sep)](https://www.reddit.com/r/PINE64official/comments/xepi5w/apache_nuttx_rtos_online_workshop_pinecone_bl602/). _If you're interested in the PineCone, save that date!_ 
* [Mepo released on Flathub,](https://flathub.org/apps/details/com.milesalan.mepo) *Exciting to see Mepo maps steadily march toward a 1.0 release!*
- PINE64official (Reddit): [China is Closed for National Holiday Oct. 1-7. if your stuff doesn't ship before then, it's Delayed](https://www.reddit.com/r/PINE64official/comments/xe81c8/china_is_closed_for_national_holiday_oct_17_if/)
- [OpenAlt](https://www.openalt.cz/2022/program.php) took place this weekend. The Czech are quite active in the mobile Linux space, so [checking out the [recorded live streams](https://www.youtube.com/c/OpenAlt/videos) and checking how well automatic translation works may be worth it!
- Purism community: [Librem 5 Daily Usage poll](https://forums.puri.sm/t/librem-5-daily-usage-poll/18227). _Go Vote!_


### Worth reading

#### How You Can Help
* Caleb Connolly: [How you can help the Linux Mobile ecosystem](https://connolly.tech/posts/2022_09_16-how-to-help-linux-mobile/)

#### Automated Phone Testing
* Martijn Braam: [Automated Phone Testing pt.2](https://blog.brixit.nl/automated-phone-testing-pt-2/)

#### Fluff
* Purism: [Thank You For Joining Us](https://puri.sm/posts/thank-you-for-joining-us/)

#### Auto Contrast
* Purism: [Auto Contrast on Librem 5 smartphones](https://puri.sm/posts/auto-contrast-on-librem-5-smartphones/). _It's actually quite useful!_


#### Portable Gaming device as a Workstation
- Carl Schwan: [Daily driving the steam deck](https://carlschwan.eu/2022/09/17/daily-driving-the-steam-deck/)

#### Important Terms
- Drew DeVaults blog: [The phrase "open source" (still) matters](https://drewdevault.com/2022/09/16/Open-source-matters.html)

#### Distractions
- Bacardis cave: [Gemlog: Limiting distraction on my phone](https://bacardi55.io/pages/gemini#5d4bfc3b5aee2b894571d416ea7c9786). _You'll need a gemini browser to read this - despite not being about mobile linux, it's IMHO worth the hassle._

#### Web Browsers
* Andreas Kling: [Ladybird: A new cross-platform browser project](https://awesomekling.github.io/Ladybird-a-new-cross-platform-browser-project/). _I did not manage to get this to work on AARCH64 yet, but it's worth watching!_

### Worth listening
* postmarketOS Podcast: [#22 Automated Phone Testing, SourceHut, Debugging Phosh, Merch](https://cast.postmarketos.org/episode/22-Automated-Phone-Testing-SourceHut-Debugging-Phosh-Merch/). _Great episode!_

### Worth watching

#### Plasma Mobile Apps
* Bitter Epic Productions: [The Apps of Plasma Mobile on my Pinephone Pro ](https://youtu.be/y0xKgxtZegc).

#### Nemo Mobile
* (RTP) Privacy Tech Tips: [First Look At Nemo Mobile On Pinephone](https://youtu.be/dgmCt8KBELg). _Nemo Mobile is sooo smooth. It just needs more manpower!_

#### Manjaro Corner
* Avisando: [PinePhone: Manjaro Phosh Beta 27: New Features and Improvements](https://youtu.be/RLF3RATxvQY)
* /u/oklopfer: [Successfully running the Gnome Mobile Shell via Manjaro on my PinePhone Pro!](https://reddit.com/r/PINE64official/comments/xgrtoj/successfully_running_the_gnome_mobile_shell_via/)

#### Fluffychat without Flatpak
* Wolf Fur Programming: [Free Script to install Fluffychat on Mobian and the pinephone Pro](https://www.youtube.com/watch?v=SGYjjLofTTs)

#### Purism Phosh Marketing
* Purism: [Auto Contrast on Librem 5 smartphones](https://www.youtube.com/watch?v=6MDumSqSxf8). _A great feature. It really helped on the beach!_

#### Droidian 
* Linux Stuff: [Droidian - Pixel 3a (Update Sep 2022)](https://www.youtube.com/watch?v=CuV76R0IsjI)

#### GNOME Shell on Mobile
* Baonks81: [GNOME Shell on mobile, gnome-mobile postmarketOS wt86047 kernel 5.18.0](https://www.youtube.com/watch?v=N1yUIJIC6CQ). _The [Redmi 2 is a Snapdragon 410 phone](https://wiki.postmarketos.org/wiki/Xiaomi_Redmi_2_(xiaomi-wt88047\)) - no performance monster. Great to see GNOME Shell Mobile run on it!_
* HUP: [Gnome Shell on mobile: gestures](https://www.youtube.com/watch?v=8biNfuqtqnc)

#### Ubuntu Touch with Waydroid
* Anino ni Kugi: [Outer Wilds Now Lives on my Devices | Ubuntu Touch](https://www.youtube.com/watch?v=HtAsrloxWYY)

#### Unboxings
* Unboxing Tomorrow: [Pine Phone: Beta Edition Unboxing](https://www.youtube.com/watch?v=24uw0dekAXA). _That software looks very, very old - it's basically two Plasma wallpapers ago old._

#### News roundups
* The Linux Experiment: [Fedora 37 Beta and GNOME Shell mobile - Linux and open source News](https://www.youtube.com/watch?v=vqW7HtHimjA)


### Thanks

Huge thanks to  

* Matthew Fennell
* [hamblingreen](https://hamblingreen.gitlab.io), *link collecting pupil*

and possible further anonymous contributors for helping with this weeks update - and to Plata for [a nifty set of Python scripts](https://framagit.org/linmob/linmob.frama.io/-/merge_requests/5) that speeds up collecting links from feeds by a lot. 

### Something missing? Want to contribute?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please get in touch via social media or email - or just put it into [the hedgedoc pad](https://pad.hacc.space/7yCLy5a9QyOLWusIFiTt9A) for the next one!

PS: In case you are wondering about the title [...](https://fosstodon.org/@linmob/108516506484897358)

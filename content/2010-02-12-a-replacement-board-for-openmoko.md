+++
title = "A replacement board for Openmoko featuring more processing power? Possibly."
aliases = ["2010/02/12/a-replacement-board-for-openmoko-featuring-more"]
author = "peter"
comments = true
date = "2010-02-12T10:13:00Z"
layout = "post"
[taxonomies]
tags = ["3g", "beagle board", "gta02", "GTA03", "openmoko", "rumors"]
categories = ["hardware", "shortform",]
authors = ["peter"]
+++
This is nothing like confirmed, and I've got no idea who exactly is behind this project and what's its name but I just stumbled upon <a href="http://lists.openmoko.org/pipermail/community/2010-February/060152.html">a posting on the Openmoko Community ML by FSO's Michael Lauer</a> stating that there is a &#8220;project that tries to supply an PCB-replacement for<br />existing GTA02&#8221;. And yeah, UMTS is there and it is supposed to deliver &#8220;roughly the power of a Beagle Board&#8221;. While this is not as interesting as another open phone would be (replacing the small GTA02 screen with its even smaller finger touchable area, this still sounds pretty cool.

BTW: The same ML posting contains some information regarding good old dead GTA03.

P.S.: If you are looking for the FOSDEM slides mentioned, get them <a href="http://www.linuxtogo.org/~mickeyl/FOSDEM2010.pdf">there</a>.

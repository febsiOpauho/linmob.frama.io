+++
title = "Weekly GNU-like Mobile Linux Update (38/2022): GNOME 43, Waydroid progress on PinePhone Pro and tow-boot updates via fwupd"
date = "2022-09-25T21:30:00Z"
updated = "2022-09-26T22:06:00Z"
draft = false
[taxonomies]
tags = ["PinePhone", "PinePhone Pro", "GNOME Shell", "Ubuntu Touch", "Sailfish OS", "fwupd", "tow-boot", "Waydroid",]
categories = ["weekly update"]
authors = ["peter"]
[extra]
author_extra = "with help by Matthew Fennell, hamblinggreen and friendly assistance from plata's awesome script"
update_note="Made tow-boot story more clear."
+++
The big stories are in the title, make sure to read 'Worth noting' this week.
<!-- more -->
_Commentary in italics._

### Hardware
* Liliputing: [DevTerm portable terminal is now available with a Raspberry Pi CM4 for $279](https://liliputing.com/devterm-portable-terminal-is-now-available-with-a-raspberry-pi-cm4-for-279/). _Yet another option to power the DevTerm!_

### Software progress

#### GNOME ecosystem
- [GNOME 43 Release Notes](https://release.gnome.org/43/). _An impressive release, landing in your favorite distribution soon - and [homework for me](https://framagit.org/linmobapps/linmobapps.frama.io/-/issues/21)._
- This Week in GNOME: [#62 Forty-three!](https://thisweek.gnome.org/posts/2022/09/twig-62/). _News Flash 2.0 is definitely worth a look, and there's even more!_
- Hubert Figuière: [Introducing Compiano](https://www.figuiere.net/hub/wlog/introducing-compiano/). _Maybe not for phones, but still interesting._
- chergert: [GNOME Builder 43.0](https://blogs.gnome.org/chergert/2022/09/22/gnome-builder-43/)
- GTK blog: [Inside the GTK font chooser](https://blog.gtk.org/2022/09/19/inside-the-gtk-font-chooser/)
- Aman Kumar: [GSoC 2022 with GNOME: Final Report](https://dev.to/amankrx/gsoc-2022-with-gnome-final-report-2hf7). _(About GNOME Health.)_

#### Plasma/Maui ecosystem
- Nate Graham: [This week in KDE: yo dawg, I heard you wanted stability](https://pointieststick.com/2022/09/23/this-week-in-kde-yo-dawg-i-heard-you-wanted-stability/)
- Qt Blog: [Qt Quick Controls 2: iOS Style](https://www.qt.io/blog/qt-quick-controls-2-ios-style)

#### Sailfish OS
- flypig: [Sailfish Community News, 22nd September, Forum Apps](https://forum.sailfishos.org/t/sailfish-community-news-22nd-september-forum-apps/13028)

#### Nemo Mobile
- neochapay on twitter: [On the way to the best music player in the world. #nemomobile #glacier-ux](https://twitter.com/neochapay/status/1573275614840492034#m)

#### Ubuntu Touch 
- UBports News: [This is not a joke. It's your biweekly UBports newsletter!](http://ubports.com/blog/ubports-news-1/post/this-is-not-a-joke-it-s-your-biweekly-ubports-newsletter-3866)

#### Distributions
- Breaking updates in pmOS edge: [Various aarch64 segfaults](https://postmarketos.org/edge/2022/09/21/various-aarch64-segfaults/)

#### Kernel
- Phoronix: [The Smaller DRM Drivers See Last Minute Updates Ahead Of Linux 6.1](https://www.phoronix.com/news/Linux-6.1-Final-DRM-Misc-Next)

#### Stack
- Phoronix: [OpenJDK Java's Native Wayland Support Progressing](https://www.phoronix.com/news/Java-Native-Wayland-Progress)
- Phoronix: [Wayland's Weston 11.0 Released With HDR Display & Multi-GPU Preparations](https://www.phoronix.com/news/Wayland-Weston-11.0)
- Phoronix: [Mesa's Turnip Now Advertises Vulkan 1.3 Support](https://www.phoronix.com/news/Turnip-Does-Vulkan-1.3)
- Phoronix: [Fwupd 1.8.5 Supports More USB4 Docks, New AMD SMU Firmware Version Plugin](https://www.phoronix.com/news/Fwupd-1.8.5-Released). _The most relevant change is [this one: You can now update tow-boot with fwupd!](https://wiki.postmarketos.org/index.php?title=Fwupd&curid=3423&diff=31013&oldid=30804)._

#### Matrix
- Matrix.org: [Pre-disclosure: upcoming critical security release of Matrix SDKs and clients](https://matrix.org/blog/2022/09/23/pre-disclosure-upcoming-critical-security-release-of-matrix-sd-ks-and-clients). *Unless you use Element Web on your mobile linux device, this likely does not affect you.*
- Matrix.org: [This Week in Matrix 2022-09-23](https://matrix.org/blog/2022/09/23/this-week-in-matrix-2022-09-23) _Includes some nice updates to Nheko and Neochat._

### Worth noting
- If Phosh suddenly starts crashing after a distribution upgrade, it's likely to due to changes in GLib 2.74.0. [This patch](https://gitlab.gnome.org/World/Phosh/phosh/-/issues/840#note_1555054) has [fixed it for Mobian](https://fosstodon.org/@mobian/109046939984794851) and will likely do the same for your distribution. 
- [PCManFM-Qt file manager works quite well on Manjaro-Plasma](https://www.reddit.com/r/pinephone/comments/xhxu17/pcmanfmqt_file_manager_works_quite_well_on/) _If you're not happy with Index, here's another option._
- @FOSSingularity: ["A developer has succeeded in making an image for Waydroid for the Pinephone pro... with full graphics acceleration!](https://twitter.com/FOSSingularity/status/1571979327922905090)
- @TuxPhones: ["🔥 Work in progress: PowerVR SGX540 reverse engineering (for mainline / postmarketOS!) https://codeberg.org/Garnet/sgx540-reversing"](https://twitter.com/tuxphones/status/1573603708834922496#m)
- [Tootle (GTK Mastodon client) has been archived](https://fosstodon.org/@linmob/109043456093882985). _With zero commits in over a year and the worsening situation in Russia, this is not exactly [surprising](https://mastodon.social/@bleakgrey/108222766737482411). I hope that some of the previous contributors (e.g. people with pending MRs) pick this up - maybe as a team. (If Vala is not your jam, because you prefer Oxidation, there's also [Social](https://gitlab.gnome.org/World/Social/), which sadly has been dormant for a long, long time and could use some re-viving, too.)_

### Worth reading
* Codethink: [GNOME OS & Atomic Upgrades on the PinePhone](https://www.codethink.co.uk/articles/2022/gnome-os-mobile/) _Great write-up!_
* Lup Yuen Lee: [NuttX RTOS for Pinephone: Blinking the LEDs](https://lupyuen.github.io/articles/pio)

### Worth listening
- [GNOME 43 RELEASED! by Linux, Daily.](https://anchor.fm/niccolove/episodes/GNOME-43-RELEASED-e1o752k)
- [I Talk Too Much About KDE. by Linux, Daily.](https://anchor.fm/niccolove/episodes/I-Talk-Too-Much-About-KDE-e1o3pmb)

### Worth watching

#### Turning things off automatically
- (RTP) Privacy Tech Tips: [WiPri -w: Turns Off WiFi Automatically When You Leave Home](https://www.youtube.com/watch?v=dl7Vu0TIyrY)

#### postmarketOS on Poco F1
- Ivon Huang [Netboot: Live booting postmarketOS without flashing rootfs (Xiaomi Poco F1)](https://www.youtube.com/watch?v=U_BVX9eL5c0)

#### Gaming on Poco F1
- Ivon Huang: [用Box86在Linux手機上執行Steam遊戲](https://www.youtube.com/watch?v=JFUp6Bq2fjg)

#### Delays make people sad
- Techlore: [I Regret Buying a Librem 5 - Here's Why!](https://www.youtube.com/watch?v=prNeLzMbytU)  _TL;DW: shipping delays._

#### Ubuntu Touch
- FilipKarc: [Privilege Escalation in Ubuntu Touch 16.04 - CVE-2022-40297 (Ethical Hacking)](https://www.youtube.com/watch?v=_v7gGTpLrj4). _I knew that this was the case, but I didn't know that this is worth a CVE. Generally: Make sure to use longer PINs or configure sudo so that it requires with distinct root password._
- NOT A FBI Honey pot: [Ubuntu touch be like..... #shorts](https://www.youtube.com/watch?v=a99YeOKeVlA)
- TechX: [The Strangest Way to Revive a Bricked Xiaomi Redmi Note 9 Pro... Ubuntu Touch Installer?!?](https://www.youtube.com/watch?v=g1Q1gni6G9M)

#### Sailfish OS
- Microsoft Continuum Gaming: [Microsoft Continuum Gaming E331: Sailfish OS Custom keyboards and Emojis](https://www.youtube.com/watch?v=3v8inREcjqc)

#### Tablets
- Baonks 81: [ArchLinux ARM on Nexus 7 2012 grouper rev.E1565 MATE ttyescape kernel-5.19.0-rc8 pmOS](https://www.youtube.com/watch?v=3noMXO93GF4)
- Antoni Aloy Torrens: [PostmarketOS Plasma Mobile 25th Anniversary on Samsung Galaxy Tab 2 7.0 tablet (PowerVR SGX 540 GPU)](https://www.youtube.com/watch?v=7DCXtpDmrPQ)
- Antoni Aloy Torrens: [PostmarketOS Phosh 0.14.0 on Samsung Galaxy Tab 2 7.0 tablet (PowerVR SGX 540 GPU)](https://www.youtube.com/watch?v=uG7LSFHO_Vg)

#### Conference Talks
- OpenAlt: [Konference OpenAlt - 18. 9. 2022 - místnost E112](https://www.youtube.com/watch?v=iv0OikILa_k). _Check the chapters._
- OpenAlt: [Konference OpenAlt - 18. 9. 2022 - místnost E105](https://www.youtube.com/watch?v=CFuRzb8ivzQ)

### Thanks

Huge thanks to  

* Matthew Fennell
* [hamblingreen](https://hamblingreen.gitlab.io), *still figuring out how to categorize stuff*

and possible further anonymous contributors for helping with this weeks update - and to Plata for [a nifty set of Python scripts](https://framagit.org/linmob/linmob.frama.io/-/merge_requests/5) that speeds up collecting links from feeds by a lot. 

### Something missing? Want to contribute?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please get in touch via social media or email - or just put it into [the hedgedoc pad](https://pad.hacc.space/7yCLy5a9QyOLWusIFiTt9A) for the next one!

PS: In case you are wondering about the title [...](https://fosstodon.org/@linmob/108516506484897358)


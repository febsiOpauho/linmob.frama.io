+++
title = "Pre3 - HP's last webOS smartphone"
aliases = ["2011/10/pre3-hps-last-webos-smartphone.html"]
date = "2011-10-23T08:46:00Z"
[taxonomies]
tags = ["comparison", "HP Pre3", "webOS", "webOS 2.1.0", "webOS 2.2.0", "webOS 3.0"]
categories = ["hardware", "impressions"]
authors = ["peter"]
+++

_About a month ago I felt like I needed some new hardware, and as I had the money and could buy it rather cheap (not at fire sale price, but close), I got myself a Pre3. I received it on Friday, 11th of September. This is based on my experiences using this device._
<!-- more -->
Buying a device that is pretty much abandoned ahead of launch sounds like an insane stunt. Buying a device that will most likely disappoint one in one way or another sounds even more stupid (it's a reality to me though, I haven't been really happy with any device since the T-Mobile G1)&mdash;I did it.Taking the device out of its box (which is rather huge compared to the box of other smartphones today, e.g. the Samsung Galaxy SII), you'll find everything you found in a box with the previous Pre devices, which were all still released under the Palm brand&mdash;only the pouch is missing.

### Hardware

Powered by a second generation Snapdragon S2 chip (QSD8255) clocked at 1,4GHz, accompagnied by 512MB of ram, which is, while not dual core, not too far off from the competition (Sony Ericssons top notch Android offerings do use the same chip, alike do all new &#8220;Mango&#8221; Windows Phone 7.5 smartphones), the outsides are probably more interesting. Looked at it on pictures, it resembles the Pre2  a lot&mdash;on a first look. But it differs, the 1230mAh battery powered Pre3 (111x64x16mm, weight: 156g) has grown, mostly in length, in comparison to its predecessors (Pre2: 101x60x17mm, weight: 135g), and so, fortunately, has the display, which is at least in terms of pixel density catching up with most of the Android crowd. With 3,6&#8221; and WVGA (800x480px) resolution, the LCD has grown and improved in terms of pixel density, while keeping at least the same quality (good colours, black blacks). At 156 grams the Pre3 is rather on the heavy side, but unless you are using it alongside a really light, huge smartphone like the Galaxy S2 you will likely barely notice.

The hardware portrait keyboard became a little bigger as well, and not only that, it became clickier, too, making it more usable, more usable, alike has become the camera, which is now bumped to 5 megapixels, including auto focus and 720p video recording. 

Besides that, the Pre3 now has a soft touch to it, the sides of it changed which is best illustrated with a photo instead of text, the slider is at least ok, if not great . it is snappy and solid. The screen is behing gorilla glass now, with softer edges to it than the Pre2 had, making the Pre3 a pleasure to touch, a device you want to hold in your hand.

The Pre3 has been shipped in two variants: One with 8GB storage (which is being reviewed here), the other with 16GB of place for your apps, photos, music and files&mdash;both not expandable. - 

### Software

The iteration of HP's webOS running shipped with the Pre3 is numbered 2.2 and has a few new features, like Skype integration, in comparison to webOS 2.1 which is available for all the Palm legacy devices exept the original Pre and which also runs on the only other HP branded webOS phone, the tiny Veer. Except Skype and&mdash;interesting for TouchPad users&mdash;the Touch to Share functionality, the webOS 2.2 on the Pre3 doesn't offer virtually anything more, in a good way and more, in a bad way.

webOS on the Pre3 feels less slugish than on any of the previous webOS hardwares, but mostly this is due to the spec'ed up hardware. Still, you'll notice some lags every once in a while, but you've got these on other platforms too, and sometimes they appear due to rather poor software, which are largely not only due to the way webOS does things[^1], but partly due to poor applications, too.

But that's not to be discussed, yet. It's rather time to start talking about the apps that are in the package by default. And aside Bing Maps, which replaces the previously included Google Maps, and while not (imho) delivering better maps is a better app, having a more accessible, faster UI, which is largely due to it is built on the new Enyo software development framework, there is nothing new in the app space. The mail app, just to name an example, still sucks badly, no threaded email yet, and there is still no such thing as virtual keyboard.

While there are plenty of new Apps for the TouchPad, the App Catalog on the Pre3 is really poorly populated, as many developers haven't given clearance for their apps to run on the Pre3 after HP killed this device ahead of launch (in order to have the app scaling correctly on the Pre3s' larger (mostly higher) screen, you just need to add in a line of code)[^2]. So if you are really considering to buy this device (or have bought it already, you should defintely install Preware on it, to have access to a little more software and patches to customize your Pre3 a little bit.

### Wrap Up

The Pre3 is a device that is nice as is. The screen is nice, beside it is a bit prone to become smudgy&mdash;it is all in all a bit awesome even. However, running webOS at not as much available Ram[^3], using a certain array of apps I ran in that infamous &#8220;too many cards&#8221;-error way to often with only one app open&mdash;since I replaced that app (which I will not name here) with another one, Other than that and issues that have been present on previous phones and are likely Mojo related, and have yet not been fixed.

Even more odd, considering we live in 2011, the Pre3 sucks at storage. Sorry for the harsh words, but these days, having 5.something gigabytes of storage available on a freshly bought product really is a bad thing in 2011, especially if you are not able to extend this inbuilt memory by plugging in a microSD card. There is a 16GB version, but it's not necessarily available in your region. 16GB for the smaller version (or maybe even 32GB, as Nokia did in 2009 with their N900) would have been the way to go here.

Overall, unfortunately, while I like the appeal of the (not totally competitive in top notch (thickness, internals and, especially, storage)) hardware. Besides that, the hardware imho suffers from poor softwar,e. While that cards multitasking is still awesome and the reason I use this device, Android 4.0 (Ice Cream Sandwich) is not more that far away in terms of multitasking and better at many other tasks[^4], and this alone explains, how I feel about this product. 



The Pre3 is a sad last product, released by an HP that is going through a lot of change without any real reason. It's a product, that while delayed, still doesn't feel quite ready&mdash;it might have felt ready with webOS 3.something, but it remains to be seen, if that is going to happen. It didn't happen fast enough, that's probably because that's that (and likely: it) with webOS. 

 Should you buy it? Unless you are webOS fanboy, or you've got a TouchPad and are a heavy user of it (that touch to share thing allows for painless bluetooth pairing and answering your texts and calls using your TouchPad), I'd seriously advise against buying a Pre3. 

### Other Reviews of the HP Pre3

* <a href="http://www.engadget.com/2011/09/24/pre-3-for-atandt-review/">Darren Murph's review of the Pre3 for AT&amp;T for engadget.com</a>
* <a href="http://www.precentral.net/review-hp-pre3">Derek Kessler's review of the HP Pre3 for precentral.net</a>


[^1]: The not that optimized web platform and the rather slow Mojo software platform (which is based on prototype, which is on the slow side of JavaScript frameworks according to web developers, but allows for object oriented programming), and the fact that it just isn't optimized&mdash;things seem to be getting better with webOS 3.*, but do not seem perfectly optimized yet, and it remains to be seen, what will happen in the future, if there is one for webOS at all.

[^2]: <code>&lt;meta name='viewport' content='height=device-height'&gt;</code>

[^3]: Because of Qualcomms way of integrating things, the TI OMAP chips in previous Pre devices had a tad more memory available.

[^4]: This is dreaming of a port that will likely not ever happen (though I am going to start a Google Code project full of thoughts of how to do this, because this is everything I have to offer).

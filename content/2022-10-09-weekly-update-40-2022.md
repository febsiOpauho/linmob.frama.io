+++
title = "Weekly GNU-like Mobile Linux Update (40/2022): Just a minor one"
date = "2022-10-09T19:50:00Z"
draft = false
[taxonomies]
tags = ["Librem 5","Nemo Mobile","Maui","LinuxPhoneApps",]
categories = ["weekly update"]
authors = ["peter"]
[extra]
author_extra = "(with friendly assistance from plata's awesome script)"
+++
This iteration of the Weekly Update contains less comments than usually.
<!-- more -->
_Commentary in italics._

### Software progress

#### GNOME ecosystem
- This Week in GNOME: [#64 Everything Green Again!](https://thisweek.gnome.org/posts/2022/10/twig-64/). _Zap looks exciting!_
- Claudio Saavedra: [Mon 2022/Oct/03](https://blogs.igalia.com/csaavedra/news-2022-10.html#D03)
- MobileLinux: [GNOME Extension Manager 0.4: Going mobile!](https://www.reddit.com/r/mobilelinux/comments/xx99xj/gnome_extension_manager_04_going_mobile/)

#### Plasma/Maui ecosystem
- Nate Graham: [These weeks in KDE: Akademy and Plasma 5.26](https://pointieststick.com/2022/10/08/these-weeks-in-kde-akademy-and-plasma-5-26/)
- mauikit.org: [Maui Report 19](https://mauikit.org/blog/maui-report-19/)
- Nate Graham: [Automate and systematize all the things!](https://pointieststick.com/2022/10/04/automate-and-systematize-all-the-things/)

#### Nemo Mobile
- neochapay on twitter: [RT by @neochapay: It's #Hacktoberfest2022! Contribute to @NemoMobile and other #FOSS projects during #Hacktober! I guess we might organize some #hackathon later this month.](https://twitter.com/xmlich02/status/1576892331726614529#m)

#### Sailfish OS
- flypig: [Sailfish Community News, Sailfish OS 4.4.0.72, Moctober](https://forum.sailfishos.org/t/sailfish-community-news-sailfish-os-4-4-0-72-moctober/13187)

#### Kernel
- Phoronix: [More Arm SoCs, Smartphones & NVIDIA Control Backbone Bus Enabled With Linux 6.1](https://www.phoronix.com/news/Linux-6.1-Arm-Hardware)
- Phoronix: [Linux 6.1 Will Likely Be This Year's LTS Kernel Release](https://www.phoronix.com/news/Linux-6.1-Likely-LTS)

#### Stack
- Phoronix: [Rusticl Shows Great Start For Rust OpenCL In Mesa - Might Support SYCL In The Future](https://www.phoronix.com/news/Rusticl-2022-XDC-State)

#### Matrix
- matrix.org: [This Week in Matrix 2022-10-07](https://matrix.org/blog/2022/10/07/this-week-in-matrix-2022-10-07)

### Worth noting
- MobileLinux: [Mobile GNOME on a Samsung Galaxy S5](https://www.reddit.com/r/mobilelinux/comments/xv1v1t/mobile_gnome_on_a_samsung_galaxy_s5/)

### Worth reading
* megi's PinePhone Development Log: [2022–10–08: Pinephone UART HW Issue](https://xnux.eu/log/#075)
* fossphones.com: [Linux Phone News - October 5, 2022](https://fossphones.com/10-05-22.html)
- LinuxPhoneApps.org: [New apps of LinuxPhoneApps.org, Q3/2022 - 400! 🎉](https://linuxphoneapps.org/blog/new-listed-apps-q3-2022-400/)
- Ahmad Samir: [Git](https://ahmadsamir.github.io/posts/13-git.html)
- LINux on MOBile: [Contributing for (advanced?) Dummies](https://linmob.net/contributing-for-advanced-dummies/). _Please note: [There's so much that can be done entirely without coding!](https://navendu.me/posts/non-code-contributions-to-open-source/)_
- Purism: [Librem 5- Device Overview](https://puri.sm/posts/librem-5-device-overview/)

### Worth watching
- Bitter Epic Productions: [Tow-Boot on your Linux Phone - Why Should you Install It and How To Do It](https://www.youtube.com/watch?v=KP0VHfLAyoY)
- Purism: [Librem 5 - Device Overview](https://www.youtube.com/watch?v=c2dhVDkSnik)
- Linux Lounge: [Testing Android Games on Ubuntu Touch with Waydroid!](https://www.youtube.com/watch?v=3F0OoKjcWZw)
- DebConf Videos: [The current state of Debian on smartphones](https://www.youtube.com/watch?v=L8ux7hr8uxA)

### Thanks

Huge thanks to Plata for [a nifty set of Python scripts](https://framagit.org/linmob/linmob.frama.io/-/merge_requests/5) that speeds up collecting links from feeds by a lot. 

### Something missing? Want to contribute?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please get in touch via social media or email - or just put it into [the hedgedoc pad](https://pad.hacc.space/7yCLy5a9QyOLWusIFiTt9A) for the next one!

PS: In case you are wondering about the title [...](https://fosstodon.org/@linmob/108516506484897358)
PPS: This one has less headlines - [what do you think?](mailto:weekly-update@linmob.net?subject=Feedback%20on%20Weekly%20Update%2039&body=Less%20or%20more%20headlines%20going%20forward%3F)


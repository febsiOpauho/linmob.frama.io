+++
title = "Weekly GNU-like Mobile Linux Update (33 and 34/2022): Always consider: There are human beings on the other side, too."
date = "2022-08-30T15:00:00Z"
updated = "2022-08-31T14:10:00Z"
draft = false
[taxonomies]
tags = ["PinePhone Pro", "PinePhone", "Librem 5", "PINE64 Community Update", "postmarketOS", "Maemo Leste", "Nemo Mobile", "Phosh", "Zero Phone", "MNT Pocket Reform",]
categories = ["weekly update"]
authors = ["peter"]
[extra]
update_note = "Fixed wrong link in section 'Dreadful stuff'. Thanks for the friendly mail that pointed this out!"
+++

A lot happened: Glodroid is now Android 13 and supports the PinePhone Pro, new releases of Manjaro Phosh and unofficial Fedora for PinePhone, a PINE64 Community Update, your Librem 5 hopefully arriving this year, Maui 2.2.0 and more!
<!-- more -->
_Commentary in italics._

### Hardware available soon
* [ZeroPhone | Crowd Supply](https://www.crowdsupply.com/arsenijs/zerophone). _2G in 2022. Hm._
* [MNT Pocket Reform | Crowd Supply](https://www.crowdsupply.com/mnt/pocket-reform).
### Software progress

#### GNOME ecosystem
* [#58 Cartographer's Delight · This Week in GNOME](https://thisweek.gnome.org/posts/2022/08/twig-58/).
* [#57 Flashy Colors · This Week in GNOME](https://thisweek.gnome.org/posts/2022/08/twig-57/).
* [GNOME 43 Beta Released With More GTK 4 Porting, Other Desktop Improvements - Phoronix](https://www.phoronix.com/news/GNOME-43-Beta).
* [Trying TypeScript for GNOME Apps – Chris's Design & Development](https://blogs.gnome.org/christopherdavis/2022/08/25/trying-typescript-for-gnome-apps/).
* [Help improve GNOME! – Form and Function](https://blogs.gnome.org/aday/2022/08/25/help-improve-gnome/). _No, not all telemetry is evil._
* [GSoC 2022: Fourth update - Code](https://ignapk.blogspot.com/2022/08/gsoc-2022-fourth-update-code.html). _This is work on Nautilus._
* [GUADEC 2022 Happenings – Phaedrus Leeds' GNOME Blog](https://blogs.gnome.org/mwleeds/2022/08/21/guadec-2022-happenings/).

#### Plasma/Maui ecosystem
* [This week in KDE: Re-bindable mouse buttons – Adventures in Linux and KDE](https://pointieststick.com/2022/08/26/this-week-in-kde-re-bindable-mouse-buttons/).
* [This week in KDE: Dolphin Selection Mode – Adventures in Linux and KDE](https://pointieststick.com/2022/08/19/this-week-in-kde-dolphin-selection-mode/).
* [KDE Ships Frameworks 5.97.0 - KDE Community](https://kde.org/announcements/frameworks/5/5.97.0/).
* [Qt Quick and Widgets, Qt 6.4 Edition](https://www.qt.io/blog/qt-quick-and-widgets-qt-6.4-edition).
* [KDE Gear 22.08 - KDE Community](https://kde.org/announcements/gear/22.08.0/).
* [KDE Snap Packages now on ARM and KDE Invent CI – Jonathan Esk-Riddell's Diary](https://jriddell.org/2022/08/26/kde-snap-packages-now-on-arm-and-kde-invent-ci/). _More ARM, yay!_
* Volker Krause: [20 Years of KDE Contributions](https://www.volkerkrause.eu/2022/08/29/kde-20-year-contribution-anniversary.html).
* [Qt Group Expanding Beyond Just The Toolkit Into More QA Software With New Acquisition - Phoronix](https://www.phoronix.com/news/Qt-Axivion).
* [Maui Project (@maui\_project): "Maui 2.2.0 is now ready. The new release announcement is coming out soon! #mauikit #maui #nitruxos"](https://twitter.com/maui_project/status/1563206940284846081#m).

#### Ubuntu Touch
* [Codemaster Freders (@fredldotme): "How about QR code scanning in the default UT Camera app? It always scans and pops up the button for details if the user is interested."](https://twitter.com/fredldotme/status/1563114671816658944#m)

#### Sailfish OS
* flypig: [Sailfish Community News, 25th August, Coffee](https://forum.sailfishos.org/t/sailfish-community-news-25th-august-coffee/12785).

#### Nemo Mobile
* [neochapay (@neochapay): "Continue work on nemo keyboard. Add digit layout, add support of url/email layouts, add extended charset select. #nemomobile #glacierUX"](https://twitter.com/neochapay/status/1562482923781365760#m)

#### Capyloon

#### Distributions
* [State of Droidian - Week 32, 2022 | Droidian](https://droidian.org/blog/state-of-droidian-week-32-2022/).
* [Release Beta 26 · manjaro-pinephone/phosh · GitHub](https://github.com/manjaro-pinephone/phosh/releases/tag/beta26).
* [Release Release #12: Aug 23, 2022 · nikhiljha/pp-fedora-sdsetup · GitHub](https://github.com/nikhiljha/pp-fedora-sdsetup/releases/tag/v0.7.0). _New unofficial Fedora!_

#### Non-Linux
* [PinePhone boots Apache NuttX RTOS](https://lupyuen.github.io/articles/uboot#u-boot-bootloader). _Lup Yuen has been doing a lot of work to bring Apache NuttX to the PinePhone._

#### Matrix
* [This Week in Matrix 2022-08-26 | Matrix.org](https://matrix.org/blog/2022/08/26/this-week-in-matrix-2022-08-26)
* [This Week in Matrix 2022-08-19 | Matrix.org](https://matrix.org/blog/2022/08/19/this-week-in-matrix-2022-08-19)
* [The Matrix Summer Special 2022 | Matrix.org](https://matrix.org/blog/2022/08/15/the-matrix-summer-special-2022)


#### Kernel and other stack improvements
* megi's PinePhone Development Log: [Sound on Pinephone Pro](https://xnux.eu/log/#074).
* [Linux 6.0-rc3 Released In Marking 31 Years Since Linus Torvalds Announced It - Phoronix](https://www.phoronix.com/news/Linux-6.0-rc3-Released)
* [Imagination PowerVR Rogue DRM Linux Kernel Driver Out For Review - Phoronix](https://www.phoronix.com/news/PowerVR-DRM-Kernel-Driver-RFC)
* [Experimental Patches Allow Much Faster AArch64 & RISC-V Kexec Kernel Reboots - Phoronix](https://www.phoronix.com/news/Linux-Faster-ARM64-RISC-V-Kexec)
* [Lenovo Yoga C630 Snapdragon Laptop Seeing Fresh Linux Improvements - Phoronix](https://www.phoronix.com/news/Lenovo-Yoga-C630-Linux-EC)

#### Android
* [Android 13 Sources Released To AOSP - Phoronix](https://www.phoronix.com/news/Android-13-AOSP).
* [Release GloDroid v0.8.0 ( Android-13 ) · GloDroid/glodroid\_manifest · GitHub](https://github.com/GloDroid/glodroid_manifest/releases/tag/v0.8.0). _This release also supports the PinePhone Pro, so if you want to run Android 13 on your PinePhone or PinePhone Pro, you can._

### Worth noting
* [niko (@nikodunk): "Gnome-shell mobile updates are about to be presented at the https://prototypefund.de/demo-day/ in Berlin on Wednesday"](https://twitter.com/nikodunk/status/1564318177206476800#m), [project page](https://prototypefund.de/project/gnome-shell-mobile/).
* megi's PinePhone Development Log: [USB recovery/hacking tool for Pinephone Pro](https://xnux.eu/log/#073). _This is a great tool for people that hate owning microSD cards!_
  * Liliputing: [This software lets you install operating systems on the PinePhone Pro without a microSD card](https://liliputing.com/this-software-lets-you-install-operating-systems-on-the-pinephone-pro-without-a-microsd-card/).

### Worth reading


#### PINE64 Community Update
* PINE64: [August update: RISC and reward](https://www.pine64.org/2022/08/28/august-update-risc-and-reward/). _I love that Nano SIM re-design for the PinePhone Pro! (If it came to the OG PinePhone, I would totally order another one.)_

#### A Letter of Resignation
* Martijn Braam: [Why I left PINE64](https://blog.brixit.nl/why-i-left-pine64/). _Martijn has done a of work for PINE64, including creating Manjaro Factory images for PinePhones (it's a lot more, but since he does not go into detail, I won't either). I get why he decided to quit (fighting the same fight twice is just fuel for burning out), and hope that he keeps enjoying contributing to postmarketOS :)_
  *  [HN Comment Thread](https://news.ycombinator.com/item?id=32494659). _I found [this comment](https://news.ycombinator.com/item?id=32510546) particularily refreshing._
  * [Martijn Braam stepping away from Pine64 : PinePhoneOfficial](https://www.reddit.com/r/PinePhoneOfficial/comments/wqppum/martijn_braam_stepping_away_from_pine64/)


#### A maybe not perfect reply to that
* Marek Kraus for PINE64: [A response to Martijn’s blog](https://www.pine64.org/2022/08/18/a-response-to-martijns-blog/). _Marek is PINE64's new community manager, since Lukasz Erecinski moved on to run the [pine64eu Store](https://pine64eu.com). I've read this many reply many times, and I think it's better than I initially thought it is. We need to consider that Marek (Gamiee) and also others at PINE64, like TL Lim are working hard on PINE64 - I recall how burnt out Lukasz seemed back when I did PineTalk, and with more products and a growing community, I doubt the job has gotten easier since. I am sure navigaating the component shortages with only very modest price increases has also not been a particularily relaxing endeavour. Considering all this, sentences like "Differences of opinion and unwillingness to listen are two different things and cannot be equated." sound very different. I firmly believe that the most important thing, especially after this long period of not meeting in the meatspace and mostly communicating in text form (that still is not entirely over), is to - whenever possible - take a step back and remember that there are, in almost every case, well-meaning humans on the other end of the line._
  * [HN Comment Thread](https://news.ycombinator.com/item?id=32507912).
  * [Pine64's response to Martijn’s blog : PinePhoneOfficial](https://www.reddit.com/r/PinePhoneOfficial/comments/wrlduq/pine64s_response_to_martijns_blog/)
* [Pine64's response to Martijn’s blog : PINE64official](https://www.reddit.com/r/PINE64official/comments/wrld5f/pine64s_response_to_martijns_blog/)


#### Pouring gasoline in the fire
* Drew DeVault: [PINE64 has let its community down](https://drewdevault.com/2022/08/18/PINE64-let-us-down.html). _Drew [opined](https://drewdevault.com/2022/01/18/Pine64s-weird-priorities.html) before in a similar manner and has recommended PINE64 earlier, therefore it makes perfect sense for him to chime in. I wish he had chosen a less inflamatory headline._
  * [HN Comment Thread](https://news.ycombinator.com/item?id=32509239)
  * [r/PINE64official Comment Thread](https://www.reddit.com/r/PINE64official/comments/wrofh3/pine64_has_let_its_community_down/)

<details>
<summary>My two cents on the matter:</summary>
When a contributor quits a project publicly, this creates an opportunity for the project to re-think and re-evaluate past decisions. The PINE64 model has always been to create some hardware, get feedback from the community during the process of the creation of the hardware, and then have the commmunity (which can buy the hardware essentially at cost) build software for the resulting product.<br>
It's a model that does not work for everybody, but looking at my PinePhone, it's definitely a model that has led to some success. The involvement of the community means, that hardware changes don't happen out of the blue, that there are no massive changes from batch to batch, just because component B was way cheaper than the previously used component A (with your run of the mill Shenzhen brand, this does happen).<br>
Martijn has done a lot for PINE64 (I don't want to know how many hours he spent doing stuff for PINE64), and quit because he felt a lack of communication, because he did not feel properly heard. And he has issues with Manjaro, which I get. Manjaro has made questionable technical choices in past and present, they aren't the best at dealing with TLS - but the sheer number of hardware partnerships they have gained shows that they are quite good at communicating and dealing with businesses; maybe because Manjaro are a business themselves. And, after all, Pine Store Ltd. is just that: A business.<br>
Therefore, I don't expect PINE64 to drop Manjaro anytime soon (it would not magically solve all issues anyway). Instead, I hope, that PINE64  manage to distribute the weight of communicating with their community across more shoulders. Decisions need to be communicated and explained to keep the community happy. If this increases prices: Do it! Customers (people that mistake especially PINE64's non-SBC products as "normal consumer products") won't be happy without well-working communications between developer community and Pine Store. Let's all be kind to each other and work through this.
</details>

#### Un Poco postmarketOS
* Drew DeVault: [A review of postmarketOS on the Xiaomi Poco F1](https://drewdevault.com/2022/08/25/pmOS-on-xiaomi-poco-f1.html). _I did not have as much trouble during postmarketOS install._
  * [Lemmy Comment Thread](https://lemmy.ml/post/441648)

#### Something positive about PinePhone
* Jakob.space: [I Love My PinePhone — Jakob's Personal Webpage](https://jakob.space/blog/i-love-my-pinephone.html). _It's long, but it is absolutely worth reading. Especially if you are into Emacs._
  * [r/pinephone Comment Thread](https://www.reddit.com/r/pinephone/comments/wyfe27/i_love_my_pinephone/).
  * [HN Commment Thread](https://news.ycombinator.com/item?id=32605597).

#### Ubuntu Touch Impressions
* [Ubuntu Touch Review · The Kernal](https://thekernal.xyz/2022/08/20/Ubuntu-Touch-Review/).

#### Code hosting considerations
* [postmarketOS // Considering SourceHut, Part 2](https://postmarketos.org/blog/2022/08/26/considering-sourcehut-2/).

#### Important stuff
* Tobias Bernard: [Post Collapse Computing Part 1: The Crisis is Here – Space and Meaning](https://blogs.gnome.org/tbernard/2022/08/24/post-collapse-computing-1/).

#### Purism
* [Where Is My Librem 5? – Purism](https://puri.sm/posts/where-is-my-librem-5/). _I hope that they manage to deliver as promised! If I may have an additional wish: Please Purism, connect your Social Media team with your support team - there are people claiming to have not received 2017 orders, and I am inclined to believe them. This needs to be solved._
* [Swipeable Upgrade to the Librem 5 Interface – Purism](https://puri.sm/posts/swipeable-upgrade-to-the-librem-5-interface/)
* [Google Ads, Apple Ads, while Purism Adds – Purism](https://puri.sm/posts/google-ads-apple-ads-while-purism-adds/)
* [PureOS on the Librem 5 USA Summer 2022 Snapshot – Purism](https://puri.sm/posts/pureos-on-the-librem-5-usa-summer-2022-snapshot/)


### Worth listening[^1]

#### The FOSS Sustainability Problem
* [Linux Downtime – Episode 53 – Linux Downtime](https://linuxdowntime.com/linux-downtime-episode-53/). _Part 1._
* [Linux Downtime – Episode 54 – Linux Downtime](https://linuxdowntime.com/linux-downtime-episode-54/). _Part 2._

### Worth watching

#### Good Stuff
* Guido Günther at FROSCON 2022: [Is there Hope for Linux on Smartphones?](https://media.ccc.de/v/froscon2022-2797-is_there_hope_for_linux_on_smartphones).
   * [Slides](https://git.sigxcpu.org/cgit/talks/2022-08-froscon-is-there-hope-for-linux-on-mobile/plain/talk.pdf).


#### Dreadful Stuff
* Chris Titus Tech: [The Linux Phone](https://www.youtube.com/watch?v=dk1nwrA0yJE). _Boy, this one made me angry (my immediate reaction that focussed on how time is not measured and accurately and how it's hard to tell experiences with the PinePhone Pro apart from experiences with the PinePhone on YouTube apparently got shadow-banned). Trashing the whole ecosystem just because PinePhone Pro development is not happening fast enough and battery life is just unfair. I get dissatisfaction - but then at least be fair and title it appropriately, e.g., "The PinePhone Pro 10 months after announcement"._

#### Community Update
* PINE64: [August Update: RISC and reward](https://www.youtube.com/watch?v=jZLvyBNwHII). _Another excellent video summary by PizzaLovingNerd!_

#### Something Fun with 5G
* [media.ccc.de - OpenRAN – 5G hacking just got a lot more interesting](https://media.ccc.de/v/mch2022-273-openran-5g-hacking-just-got-a-lot-more-interesting#t=278)

#### Maemo Leste perforance improvements
* Carbon Caffenine: [Maemo Leste - 3D Update](https://www.youtube.com/watch?v=hBStZAGXvM0).

#### PinePhone impressions
* (RTP) Privacy Tech Tips: [Pinephone: Should You Get One?](https://www.youtube.com/watch?v=YNHVmFrxqis). _I agree his verdict, and agree that the PinePhone Pro is not ready yet._

#### postmarketOS booting on...
* 8finity: [PostmarketOS booting on Xiaomi Redmi Note 4G](https://www.youtube.com/watch?v=1vIgbs7RxTI).

#### Purism
* Purism: [PureOS on the Librem 5 USA Summer 2022 Snapshot](https://www.youtube.com/watch?v=bmveDMYqemQ). _I actually like this one!_
* Purism: [Swipeable Upgrade to the Librem 5 Interface](https://www.youtube.com/watch?v=rRo09DKxqjw). _In case Phosh 0.20 got installed on your phone and despite following this blog you did not know what to do._
* Purism: [What is Special About the Librem 5 USA](https://www.youtube.com/watch?v=M_VujQqYRWg).

### Something missing?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please get in touch via social media or email!

PS: In case you are wondering about the title [...](https://fosstodon.org/@linmob/108516506484897358)

_Sorry that this Update is somewhat poorly formatted and a day late._

18.

[^1]: I wanted to put these in, but only did so three hours after initial publication.

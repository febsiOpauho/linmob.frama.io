+++
title = "Hello World!"
aliases = ["2007/09/hello-world.html"]
author = "peter"
comments = true
date = "2007-09-24T17:08:00Z"
layout = "post"
[taxonomies]
categories = ["internal", "shortform"]
authors = ["peter"]
+++
This is my first post&mdash;I am a german guy, who likes linux-powered mobile phones a lot and due to this, some of these phones will be the subject here.
<!-- more -->

I had a blog over at some other place before, but it is gone now, unfortunately. If the hoster sends me my old texts, I will import them in here.

Addendum, April 15th, 2021: _This original blog had been hosted at Motorolafans.com, a forum where Motorola's Linux phones were discussed. Their blog feature just disappeared, and the contents could never be recovered. Looking back at my early posts, this was not a huge loss._
